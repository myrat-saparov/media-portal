<script>
    const videoDefaultStatus = document.querySelector('.videoDefaultStatus');
    const defaultStatusElement = document.getElementById('videoStatus'); // Get the element with id "status"
    const defaultStatus = defaultStatusElement.textContent.trim(); // Get the text content and trim whitespace
  
    videoDefaultStatus.textContent = 'Default: '+defaultStatus;
    videoDefaultStatus.style.color = defaultStatus === 'True' ? 'green' : 'red';
</script>