package db

import (
	"fmt"

	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Database struct {
	db *gorm.DB
}

type Config struct {
	Host     string
	Port     string
	Username string
	Password string
	DBName   string
	SslMode  string
	Timezone string
}

func (db Database) NewDatabase() (*gorm.DB, error) {
	err := initConfig()
	if err != nil {
		return nil, err
	}

	dsn := Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.username"),
		Password: viper.GetString("db.password"),
		DBName:   viper.GetString("db.dbname"),
		SslMode:  viper.GetString("db.sslmode"),
	}

	DSN := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s", dsn.Host, dsn.Username, dsn.Password, dsn.DBName, dsn.Port, dsn.SslMode)

	database, err := gorm.Open(postgres.Open(DSN), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	return database, nil
}

func (db Database) GetDB() *gorm.DB {
	return db.db
}

func initConfig() error {
	viper.AddConfigPath("..")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}
