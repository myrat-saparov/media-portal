CREATE TABLE admins (
    id SERIAL PRIMARY KEY,
    username VARCHAR(50) UNIQUE NOT NULL,
    email VARCHAR(100) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE "users" (
   id SERIAL PRIMARY KEY,
   name VARCHAR(50) UNIQUE NOT NULL,
   email VARCHAR(100) UNIQUE NOT NULL,
   phone VARCHAR(100) UNIQUE NOT NULL,
   password VARCHAR(255) NOT NULL

);

CREATE TABLE "lessons"(
    id SERIAL PRIMARY KEY,
    lesson_name VARCHAR(50) UNIQUE NOT NULL
);

CREATE TABLE "os"(
    id SERIAL PRIMARY KEY,
    os_name VARCHAR(50) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS books (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    release_date REAL NOT NULL,
    picture_url TEXT NOT NULL,
    size REAL NOT NULL,
    writer TEXT NOT NULL,
    book_url TEXT NOT NULL,
    lesson_id INTEGER NOT NULL,
    verification BOOLEAN NOT NULL DEFAULT true,
    check_admin BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE IF NOT EXISTS apps (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    release_date REAL NOT NULL,
    picture_url TEXT NOT NULL,
    size REAL NOT NULL,
    app_url TEXT NOT NULL,
    os_id INTEGER NOT NULL,
    verification BOOLEAN NOT NULL DEFAULT true,
    check_admin BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE IF NOT EXISTS audios (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    singer TEXT NOT NULL,
    audio_url TEXT NOT NULL,
    lesson_id INTEGER NOT NULL,
    verification BOOLEAN NOT NULL DEFAULT true,
    check_admin BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE IF NOT EXISTS videos (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    author TEXT NOT NULL,
    video_url TEXT NOT NULL,
    lesson_id INTEGER NOT NULL,
    description TEXT NOT NULL,
    verification BOOLEAN NOT NULL DEFAULT true,
    check_admin BOOLEAN NOT NULL DEFAULT false,
    cout_views INT
);

CREATE TABLE IF NOT EXISTS videolikes (
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
    video_id INTEGER REFERENCES videos(id) ON DELETE CASCADE,
    is_like BOOLEAN,
    UNIQUE(user_id, video_id)
);
