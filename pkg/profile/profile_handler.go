package profile

import (
	"encoding/json"
	"fmt"
	"html/template"

	"MediaPortal_2.0/pkg/user"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

type Handler struct {
	Service
}

func NewHandler(service Service) *Handler {
	return &Handler{service}
}

func (h *Handler) Index(c *gin.Context) {
	views, err := template.ParseFiles(util.IncludeProfile("index")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	token, err := c.Cookie("user")
	if err != nil {
		fmt.Println(err)
		return
	}

	user, err := DecodeJwt(token)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["user"] = h.GetUserService(c, user.Email)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func DecodeJwt(tokenString string) (Profiles, error) {
	var newUser Profiles

	token, err := jwt.ParseWithClaims(tokenString, &jwt.MapClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(user.Key), nil
	})
	if err != nil {
		return newUser, err
	}

	if claims, ok := token.Claims.(*jwt.MapClaims); ok && token.Valid {
		payload, err := json.Marshal(claims)
		if err != nil {
			return newUser, err
		}
		err = json.Unmarshal(payload, &newUser)
		if err != nil {
			return newUser, err
		}
	} else {
		return newUser, fmt.Errorf("invalid token")
	}

	return newUser, nil
}
