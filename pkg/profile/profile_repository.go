package profile

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) GetUserByMail(c *gin.Context, mail string) (Profiles, error) {
	var profile Profiles = Profiles{}

	query := "SELECT * FROM users WHERE email=$1"

	result := r.DB.WithContext(c).Raw(query, mail).Scan(&profile)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return profile, errors.New("profile not found")
		}
		return profile, result.Error
	}

	return profile, nil
}

func (r *repository) GetAllLessons(c *gin.Context) ([]int, error) {
	var lessons []int

	query := "SELECT id FROM lessons"

	result := r.DB.WithContext(c).Raw(query).Scan(&lessons)
	if result.Error != nil {
		return nil, result.Error
	}

	return lessons, nil
}

func (r *repository) GetAllOS(c *gin.Context) ([]int, error) {
	var os []int

	query := "SELECT id FROM os"

	result := r.DB.WithContext(c).Raw(query).Scan(&os)
	if result.Error != nil {
		return nil, result.Error
	}

	return os, nil
}
