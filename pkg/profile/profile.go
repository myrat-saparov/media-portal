package profile

import "github.com/gin-gonic/gin"

type Profiles struct {
	ID    int
	Name  string `form:"username"`
	Email string `form:"email"`
}

type Lesson struct {
	ID         int
	LessonName string
}

type OS struct {
	ID     int
	OSName string
}

type Service interface {
	GetUserService(c *gin.Context, mail string) Profiles
	GetAllLessonServ(c *gin.Context) []Lesson
	GetAllOSServ(c *gin.Context) []OS
}

type Repository interface {
	GetUserByMail(c *gin.Context, mail string) (Profiles, error)
	GetAllLessons(c *gin.Context) ([]int, error)
	GetAllOS(c *gin.Context) ([]int, error)
}
