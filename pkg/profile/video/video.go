package videoProf

import "github.com/gin-gonic/gin"

type Video struct {
	ID           int    `form:"-"`
	UserID       int    `form:"-"`
	Name         string `form:"Name"`
	Author       string `form:"author"`
	Description  string `form:"Description"`
	VideoURL     string `form:"-"`
	LessonID     int    `form:"LessonID"`
	Verification bool   `form:"-"`
	CheckAdmin   bool
}

type ServVideo interface {
	GetVideoByUserID(c *gin.Context, userID int) []Video
	AddNewVideoServ(c *gin.Context, video Video) error
	GetVideoByIDServ(c *gin.Context, videoID int) Video
	UpdateVideoServ(c *gin.Context, video Video) error
	DeleteVideoServ(c *gin.Context, videoID int) error
}

type RepoVideo interface {
	GetVideoByUserIDRepo(c *gin.Context, userID int) ([]Video, error)
	AddNewVideo(c *gin.Context, video Video) error
	GetVideoByID(c *gin.Context, videoID int) (Video, error)
	UpdateVideo(c *gin.Context, video Video) error
	DeleteVideo(c *gin.Context, videoID int) error
}
