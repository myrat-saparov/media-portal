package videoProf

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type service struct {
	RepoVideo
}

func NewService(repo RepoVideo) *service {
	return &service{repo}
}

func (s service) GetVideoByUserID(c *gin.Context, userID int) []Video {
	videos, err := s.GetVideoByUserIDRepo(c, userID)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return videos
}

func (s service) AddNewVideoServ(c *gin.Context, video Video) error {
	videoURL, _, err := util.UploadFile(c, "video", "video")
	if err != nil {
		return err
	}

	video.VideoURL = "/" + videoURL

	err = s.AddNewVideo(c, video)
	if err != nil {
		return err
	}

	video.Verification = false

	return nil
}

func (s service) GetVideoByIDServ(c *gin.Context, videoID int) Video {
	video, err := s.GetVideoByID(c, videoID)
	if err != nil {
		fmt.Println(err)
		return video
	}

	return video
}

func (s service) UpdateVideoServ(c *gin.Context, video Video) error {
	oldVideo, err := s.GetVideoByID(c, video.ID)
	if err != nil {
		return err
	}

	videoURL, _, err := util.UploadFile(c, "video", "video")
	if err != nil {
		if errors.Is(err, http.ErrMissingFile) {
			video.VideoURL = oldVideo.VideoURL
		} else {
			return err
		}
	} else {
		err = os.Remove("./../" + oldVideo.VideoURL)
		if err != nil {
			return err
		}

		video.VideoURL = "/" + videoURL
	}

	err = s.UpdateVideo(c, video)
	if err != nil {
		return err
	}

	return nil
}

func (s service) DeleteVideoServ(c *gin.Context, videoID int) error {
	video, err := s.GetVideoByID(c, videoID)
	if err != nil {
		return err
	}

	err = s.DeleteVideo(c, videoID)
	if err != nil {
		return err
	}

	err = os.Remove("./../" + video.VideoURL)
	if err != nil {
		return err
	}

	return nil
}
