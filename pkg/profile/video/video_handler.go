package videoProf

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	"MediaPortal_2.0/pkg/profile"
	"MediaPortal_2.0/pkg/user"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type HandVideo struct {
	ServVideo
	profile.Service
	userService user.Service
}

func NewHandler(service ServVideo, adminServ profile.Service, userServ user.Service) *HandVideo {
	return &HandVideo{service, adminServ, userServ}
}

func (h HandVideo) VideoIndex(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			lessonName := h.userService.GetLessonNameServ(c, lessonID)
			return lessonName
		},
	}).ParseFiles(util.IncludeProfile("video")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	token, err := c.Cookie("user")
	if err != nil {
		fmt.Println(err)
		return
	}

	user, err := profile.DecodeJwt(token)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["videos"] = h.GetVideoByUserID(c, user.ID)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandVideo) VideoAdd(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			return h.userService.GetLessonNameServ(c, lessonID)
		},
	}).ParseFiles(util.IncludeProfile("video/add")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["lessons"] = h.GetAllLessonServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandVideo) AddNewVideo(c *gin.Context) {
	var video Video
	token, err := c.Cookie("user")
	if err != nil {
		fmt.Println(err)
		return
	}

	user, err := profile.DecodeJwt(token)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = c.ShouldBind(&video)
	if err != nil {
		fmt.Println(err)
		return
	}

	video.UserID = user.ID
	video.Author = user.Name

	err = h.AddNewVideoServ(c, video)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/video")
}

func (h HandVideo) EditVideo(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			return h.userService.GetLessonNameServ(c, lessonID)
		},
	}).ParseFiles(util.IncludeProfile("video/edit")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	videoIDStr := c.Param("id")
	videoID, err := strconv.Atoi(videoIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["video"] = h.GetVideoByIDServ(c, videoID)
	data["lessons"] = h.GetAllLessonServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandVideo) UpdateVideo(c *gin.Context) {
	var video Video
	videoIDStr := c.Param("id")
	videoID, err := strconv.Atoi(videoIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = c.ShouldBind(&video)
	if err != nil {
		fmt.Println(err)
		return
	}

	video.ID = videoID

	err = h.UpdateVideoServ(c, video)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/video")
}

func (h HandVideo) DeleteVideo(c *gin.Context) {
	videoIDStr := c.Param("id")
	videoID, err := strconv.Atoi(videoIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = h.DeleteVideoServ(c, videoID)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/video")
}

func (h HandVideo) CheckUser(action string) gin.HandlerFunc {
	return func(c *gin.Context) {
		videoIDStr := c.Param("id")
		videoID, err := strconv.Atoi(videoIDStr)
		if err != nil {
			log.Println("Invalid video ID:", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		token, err := c.Cookie("user")
		if err != nil {
			log.Println("Failed to retrieve user cookie:", err)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		user, err := profile.DecodeJwt(token)
		if err != nil {
			log.Println("Failed to decode JWT:", err)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		video := h.GetVideoByIDServ(c, videoID)
		if video.UserID != user.ID {
			log.Printf("Unauthorized %s attempt: User %d tried to %s Video %d\n", action, user.ID, action, videoID)
			c.Redirect(http.StatusSeeOther, "/profile/video")
			c.Abort()
			return
		}

		// Authorization successful, continue processing
		c.Next()
	}
}
