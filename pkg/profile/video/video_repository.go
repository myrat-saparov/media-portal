package videoProf

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) RepoVideo {
	return &repository{db}
}

func (r repository) GetVideoByUserIDRepo(c *gin.Context, userID int) ([]Video, error) {
	var videos []Video

	query := "SELECT * FROM videos WHERE user_id = $1"
	result := r.DB.WithContext(c).Raw(query, userID).Scan(&videos)
	if result.Error != nil {
		return nil, result.Error
	}

	return videos, nil
}

func (r repository) AddNewVideo(c *gin.Context, video Video) error {
	query := "INSERT INTO videos (user_id, name, author, video_url, lesson_id, description) VALUES ($1, $2, $3, $4, $5, $6)"

	result := r.DB.Exec(query, video.UserID, video.Name, video.Author, video.VideoURL, video.LessonID, video.Description)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (r repository) GetVideoByID(c *gin.Context, videoID int) (Video, error) {
	var video Video

	query := "SELECT * FROM videos WHERE id = $1"
	result := r.DB.WithContext(c).Raw(query, videoID).Scan(&video)
	if result.Error != nil {
		return video, result.Error
	}

	return video, nil
}

func (r repository) UpdateVideo(c *gin.Context, video Video) error {
	query := "UPDATE videos SET name=$1, video_url=$2, lesson_id=$3, description=$4 WHERE id=$5"

	result := r.DB.Exec(query, video.Name, video.VideoURL, video.LessonID, video.Description, video.ID)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (r repository) DeleteVideo(c *gin.Context, videoID int) error {
	query := "DELETE FROM videos WHERE id = $1"

	result := r.DB.Exec(query, videoID)
	if result.Error != nil {
		return fmt.Errorf("failed to delete video with id %d: %w", videoID, result.Error)
	}

	if result.RowsAffected == 0 {
		return fmt.Errorf("no video found with id %d", videoID)
	}

	return nil
}
