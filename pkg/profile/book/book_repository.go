package bookProf

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) RepoBook {
	return &repository{db}
}

func (r repository) GetBookByUserIDRepo(c *gin.Context, userID int) ([]Book, error) {
	var books []Book

	query := "SELECT * FROM books WHERE user_id = $1"
	result := r.DB.WithContext(c).Raw(query, userID).Scan(&books)
	if result.Error != nil {
		return nil, result.Error
	}

	return books, nil
}

func (r *repository) AddNewBook(c *gin.Context, book Book) error {
	// SQL INSERT sorgusu oluştur
	query := "INSERT INTO books (user_id, name, release_date, picture_url, size, writer, book_url, lesson_id, verification) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"

	// Sorguyu hazırla ve veritabanına gönder
	result := r.DB.Exec(query, book.UserID, book.Name, book.ReleaseDate, book.PictureURL, book.Size, book.Writer, book.BookURL, book.LessonID, book.Verification)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (r *repository) GetBookByID(c *gin.Context, bookID int) (Book, error) {
	var book Book

	query := "SELECT * FROM books WHERE id = $1"

	result := r.DB.WithContext(c).Raw(query, bookID).Scan(&book)
	if result.Error != nil {
		return book, result.Error
	}

	return book, nil
}

func (r *repository) UpdateBook(c *gin.Context, book Book) error {
	// Create query for SQL UPDATE
	query := "UPDATE books SET name=$1, release_date=$2, picture_url=$3, size=$4, writer=$5, book_url=$6, lesson_id=$7, verification=$8 WHERE id=$9"
	result := r.DB.Exec(query, book.Name, book.ReleaseDate, book.PictureURL, book.Size, book.Writer, book.BookURL, book.LessonID, book.Verification, book.ID)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (r *repository) DeleteBook(c *gin.Context, bookID int) error {
	query := "DELETE FROM books WHERE id = $1"

	result := r.DB.Exec(query, bookID)
	if result.Error != nil {
		return result.Error
	}

	return nil
}
