package bookProf

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	"MediaPortal_2.0/pkg/profile"
	"MediaPortal_2.0/pkg/user"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type HandBook struct {
	ServBook
	profile.Service
	userService user.Service
}

func NewHandler(service ServBook, adminServ profile.Service, userServ user.Service) *HandBook {
	return &HandBook{service, adminServ, userServ}
}

func (h HandBook) BookIndex(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			return h.userService.GetLessonNameServ(c, lessonID)
		},
		"bookFormat": func(bookID int) string {
			return h.GetBookFormat(c, bookID)
		},
	}).ParseFiles(util.IncludeProfile("book")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	token, err := c.Cookie("user")
	if err != nil {
		fmt.Println(err)
		return
	}

	user, err := profile.DecodeJwt(token)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["book"] = h.GetBookByUserID(c, user.ID)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandBook) BookAdd(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			return h.userService.GetLessonNameServ(c, lessonID)
		},
	}).ParseFiles(util.IncludeProfile("/book/add")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["lessons"] = h.GetAllLessonServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandBook) AddNewBook(c *gin.Context) {
	var newBook Book
	err := c.ShouldBind(&newBook)
	if err != nil {
		fmt.Println(err)
	}

	token, err := c.Cookie("user")
	if err != nil {
		fmt.Println(err)
		return
	}

	user, err := profile.DecodeJwt(token)
	if err != nil {
		fmt.Println(err)
		return
	}

	newBook.UserID = user.ID

	err = h.AddNewBookServ(c, newBook)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/book")
}

func (h HandBook) EditBook(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			return h.userService.GetLessonNameServ(c, lessonID)
		},
	}).ParseFiles(util.IncludeProfile("/book/edit")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	bookIDStr := c.Param("id")
	bookID, err := strconv.Atoi(bookIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["lessons"] = h.GetAllLessonServ(c)
	data["book"] = h.GetBookByIDServ(c, bookID)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandBook) UpdateBook(c *gin.Context) {
	var newBook Book
	err := c.ShouldBind(&newBook)
	if err != nil {
		fmt.Println(err)
	}

	err = h.UpdateBookServ(c, newBook)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/book")
}

func (h HandBook) DeleteBook(c *gin.Context) {
	bookIDStr := c.Param("id")
	bookID, err := strconv.Atoi(bookIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = h.DeleteBookServ(c, bookID)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/book")
}

func (h HandBook) CheckUser(action string) gin.HandlerFunc {
	return func(c *gin.Context) {
		bookIDStr := c.Param("id")
		bookID, err := strconv.Atoi(bookIDStr)
		if err != nil {
			log.Println("Invalid book ID:", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		token, err := c.Cookie("user")
		if err != nil {
			log.Println("Failed to retrieve user cookie:", err)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		user, err := profile.DecodeJwt(token)
		if err != nil {
			log.Println("Failed to decode JWT:", err)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		book := h.GetBookByIDServ(c, bookID)
		if book.UserID != user.ID {
			log.Printf("Unauthorized %s attempt: User %d tried to %s book %d\n", action, user.ID, action, bookID)
			c.Redirect(http.StatusSeeOther, "/profile/book")
			c.Abort()
			return
		}

		// Authorization successful, continue processing
		c.Next()
	}
}
