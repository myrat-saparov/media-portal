package bookProf

import (
	"errors"
	"fmt"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"

	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type service struct {
	RepoBook
}

func NewService(repo RepoBook) *service {
	return &service{repo}
}

func (s service) GetBookByUserID(c *gin.Context, userID int) []Book {
	books, err := s.GetBookByUserIDRepo(c, userID)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return books
}

func (s service) AddNewBookServ(c *gin.Context, book Book) error {
	bookUrl, fileInfo, err := util.UploadFile(c, "books", "book")
	if err != nil {
		return err
	}

	book.BookURL = "/" + bookUrl

	bookSize := fileInfo.Size()
	book.Size = float32(math.Round(float64(bookSize)*100/(1024*1024))) / 100

	pictureUrl, _, err := util.UploadFile(c, "books/pictures", "bookPicture")
	if err != nil {
		return err
	}

	book.PictureURL = "/" + pictureUrl
	book.Verification = false

	err = s.AddNewBook(c, book)
	if err != nil {
		return err
	}

	fmt.Println(book.BookURL, book.PictureURL)

	log.Printf("book added: %s", book.Name)

	return nil
}

func (s service) GetBookByIDServ(c *gin.Context, bookID int) Book {
	var book Book
	book, err := s.GetBookByID(c, bookID)
	if err != nil {
		fmt.Println(err)
		return book
	}

	return book
}

func (s service) UpdateBookServ(c *gin.Context, book Book) error {
	bookIDStr := c.Param("id")
	bookID, err := strconv.Atoi(bookIDStr)
	if err != nil {
		fmt.Println(err)
		return err
	}

	book.ID = bookID

	oldBook, err := s.GetBookByID(c, book.ID)
	if err != nil {
		return err
	}
	bookUrl, fileInfo, err := util.UploadFile(c, "books", "book")
	if err != nil {
		if errors.Is(err, http.ErrMissingFile) {
			book.BookURL = oldBook.BookURL
			book.Size = oldBook.Size
		} else {
			return err
		}
	} else {
		err = os.Remove("./../" + oldBook.BookURL)
		if err != nil {
			return err
		}
		book.BookURL = "/" + bookUrl
		bookSize := fileInfo.Size()
		book.Size = float32(math.Round(float64(bookSize)*100/(1024*1024))) / 100
	}

	pictureUrl, _, err := util.UploadFile(c, "books/pictures", "bookPicture")
	if err != nil {
		if errors.Is(err, http.ErrMissingFile) {
			book.PictureURL = oldBook.PictureURL
		} else {
			return err
		}
	} else {
		err = os.Remove("./../" + oldBook.PictureURL)
		if err != nil {
			return err
		}
		book.PictureURL = "/" + pictureUrl
	}

	err = s.UpdateBook(c, book)
	if err != nil {
		return err
	}

	return nil
}

func (s service) DeleteBookServ(c *gin.Context, bookID int) error {
	book, err := s.GetBookByID(c, bookID)
	if err != nil {
		return err
	}

	err = s.DeleteBook(c, bookID)
	if err != nil {
		return err
	}

	err = os.Remove("./../" + book.BookURL)
	if err != nil {
		return err
	}

	err = os.Remove("./../" + book.PictureURL)
	if err != nil {
		return err
	}

	return nil
}

func (s service) GetBookFormat(c *gin.Context, bookID int) string {
	book, err := s.GetBookByID(c, bookID)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	bookFormatIndex := strings.LastIndex(book.BookURL, ".")
	if bookFormatIndex == -1 {
		fmt.Println("format not found")
		return ""
	}

	return book.BookURL[bookFormatIndex+1:]
}
