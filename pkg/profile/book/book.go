package bookProf

import "github.com/gin-gonic/gin"

type Book struct {
	ID           int     `schema:"-"`
	UserID       int     `schema:"-"`
	Name         string  `form:"name"`
	ReleaseDate  string  `form:"realeseDate"`
	PictureURL   string  `schema:"-"`
	Size         float32 `schema:"-"`
	Writer       string  `form:"author"`
	BookURL      string  `schema:"-"`
	LessonID     int     `form:"lessonID"`
	Verification bool    `schema:"-"`
}
type ServBook interface {
	GetBookByUserID(c *gin.Context, userID int) []Book
	AddNewBookServ(c *gin.Context, book Book) error
	GetBookByIDServ(c *gin.Context, bookID int) Book
	UpdateBookServ(c *gin.Context, book Book) error
	DeleteBookServ(c *gin.Context, bookID int) error
	GetBookFormat(c *gin.Context, bookID int) string
}

type RepoBook interface {
	GetBookByUserIDRepo(c *gin.Context, userID int) ([]Book, error)
	AddNewBook(c *gin.Context, book Book) error
	GetBookByID(c *gin.Context, bookID int) (Book, error)
	UpdateBook(c *gin.Context, book Book) error
	DeleteBook(c *gin.Context, bookID int) error
}
