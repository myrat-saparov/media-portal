package profile

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

type service struct {
	Repository
}

func NewService(repo Repository) *service {
	return &service{repo}
}

func (s *service) GetUserService(c *gin.Context, mail string) Profiles {
	var user Profiles
	user, err := s.GetUserByMail(c, mail)
	if err != nil {
		fmt.Println(err)
		return user
	}
	return user
}

func (s *service) GetAllLessonServ(c *gin.Context) []Lesson {
	lessonIDs, err := s.GetAllLessons(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var lessons []Lesson

	for _, id := range lessonIDs {
		lesson := Lesson{ID: id}
		lessons = append(lessons, lesson)
	}

	return lessons
}

func (s *service) GetAllOSServ(c *gin.Context) []OS {
	lessonIDs, err := s.GetAllOS(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var oss []OS

	for _, id := range lessonIDs {
		os := OS{ID: id}
		oss = append(oss, os)
	}

	return oss
}
