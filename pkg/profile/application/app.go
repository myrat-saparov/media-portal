package appProf

import "github.com/gin-gonic/gin"

type App struct {
	ID           int     `form:"id"`
	UserID       int     `form:"userID"`
	Name         string  `form:"name"`
	ReleaseDate  string  `form:"releaseDate"`
	PictureURL   string  `form:"pictureURL"`
	Size         float32 `form:"size"`
	AppURL       string  `form:"appURL"`
	OSID         int     `form:"osID"`
	Verification bool    `form:"verification"`
	CheckAdmin   bool
}

type ServApp interface {
	GetAppByUserID(c *gin.Context, userID int) []App
	AddNewAppServ(c *gin.Context, app App) error
	GetAppByIDServ(c *gin.Context, appID int) App
	UpdateAppServ(c *gin.Context, app App) error
	DeleteAppServ(c *gin.Context, appID int) error
	GetAppFormat(c *gin.Context, appID int) string
}

type RepoApp interface {
	GetAppByUserIDRepo(c *gin.Context, userID int) ([]App, error)
	AddNewApp(c *gin.Context, app App) error
	GetAppByID(c *gin.Context, appID int) (App, error)
	UpdateApp(c *gin.Context, app App) error
	DeleteApp(c *gin.Context, appID int) error
}
