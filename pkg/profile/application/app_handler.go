package appProf

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	"MediaPortal_2.0/pkg/profile"
	"MediaPortal_2.0/pkg/user"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type HandApp struct {
	ServApp
	profile.Service
	siteServ user.Service
}

func NewHandler(service ServApp, adminServ profile.Service, siteServ user.Service) *HandApp {
	return &HandApp{service, adminServ, siteServ}
}

func (h HandApp) AppIndex(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"osName": func(osID int) string {
			return h.siteServ.GetOSNameServ(c, osID)
		},
		"appFormat": func(appID int) string {
			return h.GetAppFormat(c, appID)
		},
	}).ParseFiles(util.IncludeProfile("application")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	token, err := c.Cookie("user")
	if err != nil {
		fmt.Println(err)
		return
	}

	user, err := profile.DecodeJwt(token)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["app"] = h.GetAppByUserID(c, user.ID)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandApp) AppAdd(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"osName": func(osID int) string {
			return h.siteServ.GetOSNameServ(c, osID)
		},
	}).ParseFiles(util.IncludeProfile("/application/add")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["os"] = h.GetAllOSServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandApp) AddNewApp(c *gin.Context) {
	var app App
	err := c.ShouldBind(&app)
	if err != nil {
		fmt.Println(err)
		return
	}

	token, err := c.Cookie("user")
	if err != nil {
		fmt.Println(err)
		return
	}

	user, err := profile.DecodeJwt(token)
	if err != nil {
		fmt.Println(err)
		return
	}

	app.UserID = user.ID

	err = h.AddNewAppServ(c, app)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/application")
}

func (h HandApp) EditApp(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"osName": func(osID int) string {
			return h.siteServ.GetOSNameServ(c, osID)
		},
	}).ParseFiles(util.IncludeProfile("application/edit")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	appIDStr := c.Param("id")
	appID, err := strconv.Atoi(appIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["app"] = h.GetAppByIDServ(c, appID)
	data["os"] = h.GetAllOSServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandApp) UpdateApp(c *gin.Context) {
	var app App
	err := c.ShouldBind(&app)
	if err != nil {
		fmt.Println(err)
		return
	}

	token, err := c.Cookie("user")
	if err != nil {
		fmt.Println(err)
		return
	}

	user, err := profile.DecodeJwt(token)
	if err != nil {
		fmt.Println(err)
		return
	}

	app.UserID = user.ID

	err = h.UpdateAppServ(c, app)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/application")
}

func (h HandApp) DeleteApp(c *gin.Context) {
	appIDStr := c.Param("id")
	appID, err := strconv.Atoi(appIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = h.DeleteAppServ(c, appID)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/application")
}

func (h HandApp) CheckUser(action string) gin.HandlerFunc {
	return func(c *gin.Context) {
		appIDStr := c.Param("id")
		appID, err := strconv.Atoi(appIDStr)
		if err != nil {
			log.Println("Invalid app ID:", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		token, err := c.Cookie("user")
		if err != nil {
			log.Println("Failed to retrieve user cookie:", err)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		user, err := profile.DecodeJwt(token)
		if err != nil {
			log.Println("Failed to decode JWT:", err)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		app := h.GetAppByIDServ(c, appID)
		if app.UserID != user.ID {
			log.Printf("Unauthorized %s attempt: User %d tried to %s app %d\n", action, user.ID, action, appID)
			c.Redirect(http.StatusSeeOther, "/profile/application")
			c.Abort()
			return
		}

		// Authorization successful, continue processing
		c.Next()
	}
}
