package appProf

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) RepoApp {
	return &repository{db}
}

func (r repository) GetAppByUserIDRepo(c *gin.Context, userID int) ([]App, error) {
	var apps []App

	query := "SELECT * FROM apps WHERE user_id = $1"
	result := r.DB.WithContext(c).Raw(query, userID).Scan(&apps)
	if result.Error != nil {
		return nil, result.Error
	}

	return apps, nil
}

func (r repository) AddNewApp(c *gin.Context, app App) error {
	query := "INSERT INTO apps (user_id, name, release_date, picture_url, size, app_url, os_id, verification) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"

	result := r.DB.Exec(query, app.UserID, app.Name, app.ReleaseDate, app.PictureURL, app.Size, app.AppURL, app.OSID, app.Verification)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (r repository) GetAppByID(c *gin.Context, appID int) (App, error) {
	var apps App

	query := "SELECT * FROM apps WHERE id = $1"
	result := r.DB.WithContext(c).Raw(query, appID).Scan(&apps)
	if result.Error != nil {
		return apps, result.Error
	}

	return apps, nil
}

func (r repository) UpdateApp(c *gin.Context, app App) error {
	query := "UPDATE apps SET user_id=?, name=?, picture_url=?, size=?, app_url=?, os_id=?, verification=?, release_date=? WHERE id=?"

	result := r.DB.Exec(query, app.UserID, app.Name, app.PictureURL, app.Size, app.AppURL, app.OSID, app.Verification, app.ReleaseDate, app.ID)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (r repository) DeleteApp(c *gin.Context, appID int) error {
	query := "DELETE FROM apps WHERE id = ?"

	result := r.DB.Exec(query, appID)
	if result.Error != nil {
		return result.Error
	}

	return nil
}
