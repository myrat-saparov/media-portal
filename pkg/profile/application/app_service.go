package appProf

import (
	"errors"
	"fmt"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"

	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type service struct {
	RepoApp
}

func NewService(repo RepoApp) *service {
	return &service{repo}
}

func (s service) GetAppByUserID(c *gin.Context, userID int) []App {
	apps, err := s.GetAppByUserIDRepo(c, userID)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return apps
}

func (s service) AddNewAppServ(c *gin.Context, app App) error {
	appUrl, appInfo, err := util.UploadFile(c, "applications", "app")
	if err != nil {
		return err
	}

	app.AppURL = "/" + appUrl
	app.Size = float32(math.Round(float64(appInfo.Size())*100/(1024*1024))) / 100

	pictureUrl, _, err := util.UploadFile(c, "applications/pictures", "appPicture")
	if err != nil {
		return err
	}

	app.PictureURL = "/" + pictureUrl

	err = s.AddNewApp(c, app)
	if err != nil {
		return err
	}

	return nil
}

func (s service) GetAppByIDServ(c *gin.Context, appID int) App {
	var app App
	app, err := s.GetAppByID(c, appID)
	if err != nil {
		fmt.Println(err)
		return app
	}

	return app
}

func (s service) UpdateAppServ(c *gin.Context, app App) error {
	var oldApp App
	appIDStr := c.Param("id")
	appID, err := strconv.Atoi(appIDStr)
	if err != nil {
		return err
	}

	oldApp, err = s.GetAppByID(c, appID)
	if err != nil {
		return err
	}

	app.ID = appID

	appUrl, fileInfo, err := util.UploadFile(c, "applications", "app")
	if err != nil {
		if errors.Is(err, http.ErrMissingFile) {
			app.AppURL = oldApp.AppURL
			app.Size = oldApp.Size
		} else {
			return err
		}
	} else {
		err = os.Remove("./../" + oldApp.AppURL)
		if err != nil {
			return err
		}
		app.AppURL = "/" + appUrl
		appSize := fileInfo.Size()
		app.Size = float32(math.Round(float64(appSize)*100/(1024*1024))) / 100
	}

	pictureUrl, _, err := util.UploadFile(c, "applications/pictures", "appPicture")
	if err != nil {
		if errors.Is(err, http.ErrMissingFile) {
			app.PictureURL = oldApp.PictureURL
		} else {
			return err
		}
	} else {
		err = os.Remove("./../" + oldApp.PictureURL)
		if err != nil {
			return err
		}
		app.PictureURL = "/" + pictureUrl
	}

	err = s.UpdateApp(c, app)
	if err != nil {
		return err
	}

	return nil
}

func (s service) DeleteAppServ(c *gin.Context, appID int) error {
	app, err := s.GetAppByID(c, appID)
	if err != nil {
		return err
	}

	err = s.DeleteApp(c, appID)
	if err != nil {
		return err
	}

	err = os.Remove("./../" + app.AppURL)
	if err != nil {
		return err
	}

	err = os.Remove("./../" + app.PictureURL)
	if err != nil {
		return err
	}

	return nil
}

func (s service) GetAppFormat(c *gin.Context, appID int) string {
	app, err := s.GetAppByID(c, appID)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	formatApp := strings.LastIndex(app.AppURL, ".")
	if formatApp == -1 {
		fmt.Println("format not found")
		return ""
	}

	return app.AppURL[formatApp+1:]
}
