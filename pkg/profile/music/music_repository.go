package musicProf

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) RepoMusic {
	return &repository{db}
}

func (r repository) GetMusicByUserIDRepo(c *gin.Context, userID int) ([]Music, error) {
	var musics []Music

	query := "SELECT * FROM audios WHERE user_id = $1"
	result := r.DB.WithContext(c).Raw(query, userID).Scan(&musics)
	if result.Error != nil {
		return nil, result.Error
	}

	return musics, nil
}

func (r repository) AddNewAudio(c *gin.Context, audio Music) error {
	query := "INSERT INTO audios (user_id, name, singer, audio_url, lesson_id, verification) VALUES (?, ?, ?, ?, ?, ?)"

	result := r.DB.Exec(query, audio.UserID, audio.Name, audio.Singer, audio.AudioURL, audio.LessonID, audio.Verification)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (r repository) GetMusicByID(c *gin.Context, audioID int) (Music, error) {
	var audio Music

	query := "SELECT * FROM audios WHERE id = $1"
	result := r.DB.WithContext(c).Raw(query, audioID).Scan(&audio)
	if result.Error != nil {
		return audio, result.Error
	}

	return audio, nil
}

func (r repository) UpdateAudio(c *gin.Context, audio Music) error {
	query := "UPDATE audios SET name=$1, singer=$2, audio_url=$3, lesson_id=$4 WHERE id= $5"

	result := r.DB.Exec(query, audio.Name, audio.Singer, audio.AudioURL, audio.LessonID, audio.ID)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (r repository) DeleteAudio(c *gin.Context, audioID int) error {
	query := "DELETE FROM audios WHERE id=$1"

	result := r.DB.Exec(query, audioID)
	if result.Error != nil {
		return result.Error
	}

	return nil
}
