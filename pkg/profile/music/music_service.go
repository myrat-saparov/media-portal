package musicProf

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type service struct {
	RepoMusic
}

func NewService(repo RepoMusic) *service {
	return &service{repo}
}

func (s service) GetMusicByUserID(c *gin.Context, userID int) []Music {
	musics, err := s.GetMusicByUserIDRepo(c, userID)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return musics
}

func (s service) AddNewAudioServ(c *gin.Context, audio Music) error {
	audioUrl, _, err := util.UploadFile(c, "music", "audio")
	if err != nil {
		return err
	}

	audio.AudioURL = "/" + audioUrl

	err = s.AddNewAudio(c, audio)
	if err != nil {
		return err
	}

	return nil
}

func (s service) GetMusicByIDServ(c *gin.Context, audioID int) Music {
	var audio Music
	audio, err := s.GetMusicByID(c, audioID)
	if err != nil {
		fmt.Println(err)
		return audio
	}

	return audio
}

func (s service) UpdateAudioServ(c *gin.Context, audio Music) error {
	oldAudio, err := s.GetMusicByID(c, audio.ID)
	if err != nil {
		return err
	}

	audioURL, _, err := util.UploadFile(c, "music", "audio")
	if err != nil {
		if errors.Is(err, http.ErrMissingFile) {
			audio.AudioURL = oldAudio.AudioURL
		} else {
			return err
		}
	} else {
		err = os.Remove("./../" + oldAudio.AudioURL)
		if err != nil {
			return err
		}
		audio.AudioURL = "/" + audioURL
	}

	err = s.UpdateAudio(c, audio)
	if err != nil {
		return err
	}

	return nil
}

func (s service) DeleteAudioServ(c *gin.Context, audioID int) error {
	audio, err := s.GetMusicByID(c, audioID)
	if err != nil {
		return err
	}

	err = s.DeleteAudio(c, audioID)
	if err != nil {
		return err
	}

	err = os.Remove("./../" + audio.AudioURL)
	if err != nil {
		return err
	}

	return nil
}
