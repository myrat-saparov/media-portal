package musicProf

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	"MediaPortal_2.0/pkg/profile"
	"MediaPortal_2.0/pkg/user"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type HandMusic struct {
	ServMusic
	profile.Service
	userService user.Service
}

func NewHandler(service ServMusic, adminServ profile.Service, userServ user.Service) *HandMusic {
	return &HandMusic{service, adminServ, userServ}
}

func (h HandMusic) MusicIndex(c *gin.Context) {
	views, err := template.ParseFiles(util.IncludeProfile("audio")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	token, err := c.Cookie("user")
	if err != nil {
		fmt.Println(err)
		return
	}

	user, err := profile.DecodeJwt(token)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["audios"] = h.GetMusicByUserID(c, user.ID)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandMusic) AudioAdd(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			return h.userService.GetLessonNameServ(c, lessonID)
		},
	}).ParseFiles(util.IncludeProfile("audio/add")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["lessons"] = h.GetAllLessonServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandMusic) AddNewAudio(c *gin.Context) {
	var audio Music
	token, err := c.Cookie("user")
	if err != nil {
		fmt.Println(err)
		return
	}

	user, err := profile.DecodeJwt(token)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = c.ShouldBind(&audio)
	if err != nil {
		fmt.Println(err)
		return
	}

	audio.UserID = user.ID

	err = h.AddNewAudioServ(c, audio)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/audio")
}

func (h HandMusic) EditAudio(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			return h.userService.GetLessonNameServ(c, lessonID)
		},
	}).ParseFiles(util.IncludeProfile("audio/edit")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	audioIDStr := c.Param("id")
	audioID, err := strconv.Atoi(audioIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["audio"] = h.GetMusicByIDServ(c, audioID)
	data["lessons"] = h.GetAllLessonServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandMusic) UpdateAudio(c *gin.Context) {
	var audio Music
	audioIDStr := c.Param("id")
	audioID, err := strconv.Atoi(audioIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = c.ShouldBind(&audio)
	if err != nil {
		fmt.Println(err)
		return
	}

	audio.ID = audioID

	err = h.UpdateAudioServ(c, audio)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/audio")
}

func (h HandMusic) DeleteAudio(c *gin.Context) {
	audioIDStr := c.Param("id")
	audioID, err := strconv.Atoi(audioIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = h.DeleteAudioServ(c, audioID)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/profile/audio")
}

func (h HandMusic) CheckUser(action string) gin.HandlerFunc {
	return func(c *gin.Context) {
		audioIDStr := c.Param("id")
		audioID, err := strconv.Atoi(audioIDStr)
		if err != nil {
			log.Println("Invalid audio ID:", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		token, err := c.Cookie("user")
		if err != nil {
			log.Println("Failed to retrieve user cookie:", err)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		user, err := profile.DecodeJwt(token)
		if err != nil {
			log.Println("Failed to decode JWT:", err)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		audio := h.GetMusicByIDServ(c, audioID)
		if audio.UserID != user.ID {
			log.Printf("Unauthorized %s attempt: User %d tried to %s audio %d\n", action, user.ID, action, audioID)
			c.Redirect(http.StatusSeeOther, "/profile/audio")
			c.Abort()
			return
		}

		// Authorization successful, continue processing
		c.Next()
	}
}
