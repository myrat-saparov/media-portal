package musicProf

import "github.com/gin-gonic/gin"

type Music struct {
	ID           int
	UserID       int
	Name         string `form:"name"`
	Singer       string `form:"singer"`
	AudioURL     string
	LessonID     int  `form:"lessonID"`
	Verification bool `form:"verification"`
	CheckAdmin   bool
}

type ServMusic interface {
	GetMusicByUserID(c *gin.Context, userID int) []Music
	AddNewAudioServ(c *gin.Context, audio Music) error
	GetMusicByIDServ(c *gin.Context, audioID int) Music
	UpdateAudioServ(c *gin.Context, audio Music) error
	DeleteAudioServ(c *gin.Context, audioID int) error
}

type RepoMusic interface {
	GetMusicByUserIDRepo(c *gin.Context, userID int) ([]Music, error)
	AddNewAudio(c *gin.Context, audio Music) error
	GetMusicByID(c *gin.Context, audioID int) (Music, error)
	UpdateAudio(c *gin.Context, audio Music) error
	DeleteAudio(c *gin.Context, audioID int) error
}
