package adminBook

import (
	"fmt"
	"sort"
	"strings"

	"github.com/gin-gonic/gin"
)

type adminBookServ struct {
	Repository
}

func NewService(adminVideRepo *adminBookRepo) *adminBookServ {
	return &adminBookServ{adminVideRepo}
}

func (s adminBookServ) GetAllBookServ(c *gin.Context) []Book {
	books, err := s.GetAllBook(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	sort.Slice(books, func(i, j int) bool {
		return !books[i].CheckAdmin && books[j].CheckAdmin
	})

	return books
}

func (s adminBookServ) GetBookFormat(c *gin.Context, bookID int) string {
	book, err := s.GetBookByID(c, bookID)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	lastIndex := strings.LastIndex(book.BookURL, ".")
	if lastIndex == -1 {
		fmt.Println("book format not found")
		return ""
	}

	return book.BookURL[lastIndex+1:]
}

func (s adminBookServ) CheckOperationServ(c *gin.Context, bookID int, status bool) error {
	return s.CheckOperation(c, bookID, status)
}
