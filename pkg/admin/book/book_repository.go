package adminBook

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type adminBookRepo struct {
	*gorm.DB
}

func NewRepository(dbConn *gorm.DB) *adminBookRepo {
	return &adminBookRepo{dbConn}
}

func (r adminBookRepo) GetAllBook(c *gin.Context) ([]Book, error) {
	var books []Book

	query := "SELECT * FROM books"

	result := r.DB.WithContext(c).Raw(query).Scan(&books)
	if result.Error != nil {
		return nil, result.Error
	}

	return books, nil
}

func (r adminBookRepo) GetBookByID(c *gin.Context, bookID int) (Book, error) {
	var book Book

	query := "SELECT * FROM books WHERE id = $1"

	result := r.DB.WithContext(c).Raw(query, bookID).Scan(&book)
	if result.Error != nil {
		return book, result.Error
	}

	return book, nil
}

func (r adminBookRepo) CheckOperation(c *gin.Context, bookID int, status bool) error {
	query := "UPDATE books SET verification = $1, check_admin = $2 WHERE id = $3"

	result := r.DB.WithContext(c).Exec(query, status, true, bookID)
	if result.Error != nil {
		return result.Error
	}

	return nil
}
