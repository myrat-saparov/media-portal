package adminBook

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"MediaPortal_2.0/pkg/admin"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type AdminBookHand struct {
	Service
	adminServ admin.Service
}

func NewHandler(adminBookServ *adminBookServ, adminServ admin.Service) *AdminBookHand {
	return &AdminBookHand{adminBookServ, adminServ}
}

func (h AdminBookHand) BookIndex(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			return h.adminServ.GetLessonName(c, lessonID)
		},
		"bookFormat": func(bookID int) string {
			return h.GetBookFormat(c, bookID)
		},
	}).ParseFiles(util.IncludeAdmin("book")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["books"] = h.GetAllBookServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h AdminBookHand) CheckOperations(c *gin.Context) {
	var status bool = false
	bookIDStr := c.Param("id")
	bookID, err := strconv.Atoi(bookIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	check := c.Param("status")
	if check == "true" {
		status = true
	}

	err = h.CheckOperationServ(c, bookID, status)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/admin/book")
}
