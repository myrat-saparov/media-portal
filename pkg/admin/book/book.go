package adminBook

import "github.com/gin-gonic/gin"

type Book struct {
	ID           int
	UserID       int
	Name         string
	ReleaseDate  string
	PictureURL   string
	Size         float32
	Writer       string
	BookURL      string
	LessonID     int
	Verification bool
	CheckAdmin   bool
}

type Service interface {
	GetAllBookServ(c *gin.Context) []Book
	GetBookFormat(c *gin.Context, bookID int) string
	CheckOperationServ(c *gin.Context, bookID int, status bool) error
}

type Repository interface {
	GetAllBook(c *gin.Context) ([]Book, error)
	GetBookByID(c *gin.Context, bookID int) (Book, error)
	CheckOperation(c *gin.Context, bookID int, status bool) error
}
