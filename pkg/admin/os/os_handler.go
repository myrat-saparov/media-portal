package os

import (
	"errors"
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"MediaPortal_2.0/pkg/admin"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type HandOS struct {
	ServOS
	admin.Service
}

func NewHandler(service ServOS, adminServ admin.Service) *HandOS {
	return &HandOS{service, adminServ}
}

func (h HandOS) OSIndex(c *gin.Context) {

	views, err := template.ParseFiles(util.IncludeAdmin("dashboard/os")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["os"] = h.GetAllOSServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandOS) AddOS(c *gin.Context) {
	var os OS
	err := c.ShouldBind(&os)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = h.AddOSServ(c, os)
	if err != nil {
		if errors.Is(err, errors.New("os name already exists")) {
			fmt.Println(err)
			http.Redirect(c.Writer, c.Request, "/admin/login", http.StatusSeeOther)
		} else {
			fmt.Println(err)
			return
		}
	}

	http.Redirect(c.Writer, c.Request, "/admin/os", http.StatusSeeOther)
}

func (h *HandOS) DeleteOS(c *gin.Context) {
	idStr := c.Param("id")

	id, err := strconv.Atoi(idStr)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = h.DeleteOSServ(c, id)
	if err != nil {
		fmt.Println(err)
		return
	}

	http.Redirect(c.Writer, c.Request, "/admin/os", http.StatusSeeOther)
}
