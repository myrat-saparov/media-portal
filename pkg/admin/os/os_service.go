package os

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

type service struct {
	RepoOS
}

func NewService(repo RepoOS) *service {
	return &service{repo}
}

func (s service) AddOSServ(c *gin.Context, os OS) error {
	return s.AddOSRepo(c, os)
}

func (s service) GetAllOSServ(c *gin.Context) []OS {
	oss, err := s.GetAllOSRepo(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return oss
}

func (s service) DeleteOSServ(c *gin.Context, id int) error {
	return s.DeleteOSRepo(c, id)
}
