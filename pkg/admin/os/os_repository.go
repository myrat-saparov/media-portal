package os

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) RepoOS {
	return &repository{db}
}

func (r repository) AddOSRepo(c *gin.Context, os OS) error {
	if os.OSName == "" {
		return errors.New("os name is empty")
	}

	check := r.DB.Where("os_name = ?", os.OSName).First(&OS{})
	if check.Error != nil {
		if errors.Is(check.Error, gorm.ErrRecordNotFound) {
			result := r.DB.WithContext(c).Create(&os)
			if result.Error != nil {
				return result.Error
			}
		} else {
			return check.Error
		}
	} else {
		return errors.New("os name already exists")
	}

	return nil
}

func (r repository) GetAllOSRepo(c *gin.Context) ([]OS, error) {
	var oss []OS

	query := "SELECT * FROM os"
	result := r.DB.WithContext(c).Raw(query).Scan(&oss)
	if result.Error != nil {
		return nil, result.Error
	}

	return oss, nil
}

func (r repository) DeleteOSRepo(c *gin.Context, id int) error {
	query := "DELETE FROM os WHERE id = $1"

	result := r.DB.WithContext(c).Exec(query, id)
	if result.Error != nil {
		return result.Error
	}

	return nil
}
