package os

import "github.com/gin-gonic/gin"

type OS struct {
	ID     int    `form:"id"`
	OSName string `form:"osName"`
}

type ServOS interface {
	AddOSServ(c *gin.Context, os OS) error
	GetAllOSServ(c *gin.Context) []OS
	DeleteOSServ(c *gin.Context, id int) error
}

type RepoOS interface {
	AddOSRepo(c *gin.Context, os OS) error
	GetAllOSRepo(c *gin.Context) ([]OS, error)
	DeleteOSRepo(c *gin.Context, id int) error
}
