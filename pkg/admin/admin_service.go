package admin

import (
	"context"
	"errors"
	"fmt"
	"time"

	"MediaPortal_2.0/pkg/user"
	"github.com/gin-gonic/gin"
)

type service struct {
	Repository
	timeOut time.Duration
}

func NewService(repo Repository) Service {
	return &service{
		Repository: repo,
		timeOut:    time.Duration(2) * time.Second,
	}
}

func (s service) LoginService(c *gin.Context, adminReq adminReq) (bool, error) {
	_, cancel := context.WithTimeout(c, s.timeOut)
	defer cancel()

	admin, err := s.GetAdminByMail(c, adminReq.Email)
	if err != nil {
		return false, err
	}

	if admin.Password != adminReq.Password {
		return false, errors.New("password incorrect")
	}

	session, err := store.Get(c.Request, "admin")
	if err != nil {
		return false, err
	}

	session.Values["mail"] = adminReq.Email
	session.Values["password"] = adminReq.Password

	err = session.Save(c.Request, c.Writer)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (s service) LogoutService(c *gin.Context) error {
	session, err := store.Get(c.Request, "admin")
	if err != nil {
		return err
	}

	session.Options.MaxAge = -1

	return session.Save(c.Request, c.Writer)
}

func (s service) GetAllUserServ(c *gin.Context) []user.Users {
	users, err := s.GetAllUsersRepository(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return users
}

func (s service) GetAdminByMailServ(c *gin.Context, mail string) (Admins, error) {
	return s.GetAdminByMail(c, mail)
}

func (s service) GetLessonName(c *gin.Context, lessonID int) string {
	lesson, err := s.GetLessonByID(c, lessonID)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	return lesson.LessonName
}

func (s service) GetOSName(c *gin.Context, osID int) string {
	os, err := s.GetOSByID(c, osID)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	return os
}
