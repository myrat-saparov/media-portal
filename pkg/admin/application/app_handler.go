package adminApp

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"MediaPortal_2.0/pkg/admin"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type AdminAppHand struct {
	Service
	adminServ admin.Service
}

func NewHandler(adminAppServ *adminAppServ, adminServ admin.Service) *AdminAppHand {
	return &AdminAppHand{adminAppServ, adminServ}
}

func (h AdminAppHand) AppIndex(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"osName": func(osID int) string {
			return h.adminServ.GetOSName(c, osID)
		},
		"appFormat": func(appID int) string {
			return h.GetAppFormatServ(c, appID)
		},
	}).ParseFiles(util.IncludeAdmin("application")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["apps"] = h.GetAllAppServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h AdminAppHand) CheckOperations(c *gin.Context) {
	var status bool
	appIDStr := c.Param("id")
	appID, err := strconv.Atoi(appIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	check := c.Param("status")
	if check == "true" {
		status = true
	} else {
		status = false
	}

	err = h.CheckOperationServ(c, appID, status)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/admin/application")
}
