package adminApp

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type adminAppRepo struct {
	*gorm.DB
}

func NewRepository(dbConn *gorm.DB) *adminAppRepo {
	return &adminAppRepo{dbConn}
}

func (r adminAppRepo) GetAllApp(c *gin.Context) ([]App, error) {
	var apps []App

	query := "SELECT * FROM apps"

	result := r.DB.WithContext(c).Raw(query).Scan(&apps)
	if result.Error != nil {
		return nil, result.Error
	}

	return apps, nil
}

func (r adminAppRepo) GetAppByID(c *gin.Context, appID int) (App, error) {
	var app App

	query := "SELECT * FROM apps WHERE id = $1"

	result := r.DB.WithContext(c).Raw(query, appID).First(&app)
	if result.Error != nil {
		return app, result.Error
	}

	return app, nil
}

func (r adminAppRepo) CheckOperation(c *gin.Context, appID int, status bool) error {
	query := "UPDATE apps SET verification=$1, check_admin=$2 WHERE id=$3"

	result := r.DB.Exec(query, status, true, appID)
	if result.Error != nil {
		return result.Error
	}

	return nil
}
