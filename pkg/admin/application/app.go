package adminApp

import "github.com/gin-gonic/gin"

type App struct {
	ID           int     `form:"id"`
	UserID       int     `form:"userID"`
	Name         string  `form:"name"`
	ReleaseDate  string  `form:"releaseDate"`
	PictureURL   string  `form:"pictureURL"`
	Size         float32 `form:"size"`
	AppURL       string  `form:"appURL"`
	OSID         int     `form:"osID"`
	Verification bool    `form:"verification"`
	CheckAdmin   bool
}

type Service interface {
	GetAllAppServ(c *gin.Context) []App
	GetAppFormatServ(c *gin.Context, appID int) string
	CheckOperationServ(c *gin.Context, appID int, status bool) error
}

type Repository interface {
	GetAllApp(c *gin.Context) ([]App, error)
	GetAppByID(c *gin.Context, appID int) (App, error)
	CheckOperation(c *gin.Context, appID int, status bool) error
}
