package adminApp

import (
	"fmt"
	"sort"
	"strings"

	"github.com/gin-gonic/gin"
)

type adminAppServ struct {
	Repository
}

func NewService(adminVideRepo *adminAppRepo) *adminAppServ {
	return &adminAppServ{adminVideRepo}
}

func (s adminAppServ) GetAllAppServ(c *gin.Context) []App {
	apps, err := s.GetAllApp(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	sort.Slice(apps, func(i, j int) bool {
		return !apps[i].CheckAdmin && apps[j].CheckAdmin
	})

	return apps
}

func (s adminAppServ) GetAppFormatServ(c *gin.Context, appID int) string {
	app, err := s.GetAppByID(c, appID)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	lastIndex := strings.LastIndex(app.AppURL, ".")
	if lastIndex == -1 {
		fmt.Println("format not found")
		return ""
	}

	return app.AppURL[lastIndex+1:]
}

func (s adminAppServ) CheckOperationServ(c *gin.Context, appID int, status bool) error {
	return s.CheckOperation(c, appID, status)
}
