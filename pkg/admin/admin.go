package admin

import (
	"MediaPortal_2.0/pkg/user"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/sessions"
)

type Admins struct {
	ID       int
	Username string `form:"username"`
	Email    string `form:"email"`
	Password string `form:"password"`
}

type adminReq struct {
	Email    string `form:"email"`
	Password string `form:"password"`
}

type Lesson struct {
	ID         int
	LessonName string
}

type OS struct {
	ID     int
	osName string `gorm:os_name`
}

type Service interface {
	LoginService(c *gin.Context, admin adminReq) (bool, error)
	LogoutService(c *gin.Context) error
	GetAdminByMail(c *gin.Context, mail string) (Admins, error)
	GetAllUserServ(c *gin.Context) []user.Users
	GetLessonName(c *gin.Context, lessonID int) string
	GetOSName(c *gin.Context, osID int) string
}

type Repository interface {
	GetAdminByMail(ctx *gin.Context, mail string) (Admins, error)
	GetAllUsersRepository(c *gin.Context) ([]user.Users, error)
	GetLessonByID(c *gin.Context, lessonID int) (Lesson, error)
	GetOSByID(c *gin.Context, osID int) (string, error)
}

var store = sessions.NewCookieStore([]byte("27072004"))
