package admin

import (
	"fmt"
	"html/template"
	"net/http"

	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	Service
}

func NewHandler(service Service) *Handler {
	return &Handler{service}
}

func (h *Handler) LoginIndex(c *gin.Context) {
	views, err := template.ParseFiles(util.IncludeAdmin("login")...)
	if err != nil {
		fmt.Println("Parse Error", err)
		return
	}

	err = views.ExecuteTemplate(c.Writer, "index", nil)
	if err != nil {
		fmt.Println("Execute err", err)
		return
	}
}

func (h *Handler) Login(c *gin.Context) {
	var newAdmin adminReq
	err := c.ShouldBind(&newAdmin)
	if err != nil {
		fmt.Println(err)
		return
	}

	check, err := h.LoginService(c, newAdmin)
	if err != nil {
		if err.Error() == "password incorrect" {
			fmt.Println("Password is incorrect. Please try again.")
		} else {
			fmt.Println(err)
			return
		}
	}
	if !check {
		http.Redirect(c.Writer, c.Request, "/admin/login", http.StatusSeeOther)
		return
	}

	http.Redirect(c.Writer, c.Request, "/admin", http.StatusSeeOther)
}

func (h *Handler) Logout(c *gin.Context) {
	err := h.LogoutService(c)
	if err != nil {
		fmt.Println(err)
		return
	}

	http.Redirect(c.Writer, c.Request, "/admin/login", http.StatusSeeOther)
}

func (h *Handler) Check(c *gin.Context) {
	session, err := store.Get(c.Request, "admin")
	if err != nil {
		c.Redirect(http.StatusSeeOther, "/admin/login")
		c.Abort()
		return
	}

	mailValue, mailExists := session.Values["mail"].(string)
	passwordValue, passwordExists := session.Values["password"].(string)

	if !mailExists || !passwordExists {
		c.Redirect(http.StatusSeeOther, "/admin/login")
		c.Abort()
		return
	}

	admin, err := h.GetAdminByMail(c, mailValue)
	if err != nil {
		c.Redirect(http.StatusSeeOther, "/admin/login")
		c.Abort()
		return
	}

	if admin.Password != passwordValue {
		c.Redirect(http.StatusSeeOther, "/admin/login")
		c.Abort()
		return
	}

	c.Next()
}

func (h *Handler) Dashboard(c *gin.Context) {
	views, err := template.ParseFiles(util.IncludeAdmin("dashboard")...)
	if err != nil {
		fmt.Println("Parse Error", err)
		return
	}

	data := make(map[string]interface{})
	data["User"] = h.GetAllUserServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println("Execute err", err)
		return
	}
}
