package adminAudio

import (
	"fmt"
	"sort"

	"github.com/gin-gonic/gin"
)

type adminAudioServ struct {
	Repository
}

func NewService(adminVideRepo *adminAudioRepo) *adminAudioServ {
	return &adminAudioServ{adminVideRepo}
}

func (s adminAudioServ) GetAllAudioServ(c *gin.Context) []Music {
	audios, err := s.GetAllAudios(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	sort.Slice(audios, func(i, j int) bool {
		return !audios[i].CheckAdmin && audios[j].CheckAdmin
	})

	return audios
}

func (s adminAudioServ) CheckOperationServ(c *gin.Context, audioID int, status bool) error {
	err := s.CheckOperation(c, audioID, status)
	return err
}
