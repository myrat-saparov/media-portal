package adminAudio

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type adminAudioRepo struct {
	*gorm.DB
}

func NewRepository(dbConn *gorm.DB) *adminAudioRepo {
	return &adminAudioRepo{dbConn}
}

func (r adminAudioRepo) GetAllAudios(c *gin.Context) ([]Music, error) {
	var audios []Music

	query := "SELECT * FROM audios"

	result := r.DB.WithContext(c).Raw(query).Scan(&audios)
	if result.Error != nil {
		return nil, result.Error
	}

	return audios, nil
}

func (r adminAudioRepo) CheckOperation(c *gin.Context, audioID int, status bool) error {
	query := "UPDATE audios SET verification=$1, check_admin=true WHERE id=$2"

	result := r.DB.Exec(query, status, audioID)
	if result.Error != nil {
		return result.Error
	}

	return nil
}
