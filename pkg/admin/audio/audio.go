package adminAudio

import "github.com/gin-gonic/gin"

type Music struct {
	ID           int
	UserID       int
	Name         string `form:"name"`
	Singer       string `form:"singer"`
	AudioURL     string
	LessonID     int  `form:"lessonID"`
	Verification bool `form:"verification"`
	CheckAdmin   bool
}

type Service interface {
	GetAllAudioServ(c *gin.Context) []Music
	CheckOperationServ(c *gin.Context, audioID int, status bool) error
}

type Repository interface {
	GetAllAudios(c *gin.Context) ([]Music, error)
	CheckOperation(c *gin.Context, audioID int, status bool) error
}
