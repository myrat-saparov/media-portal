package adminAudio

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"MediaPortal_2.0/pkg/admin"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type AdminAudioHand struct {
	Service
	adminServ admin.Service
}

func NewHandler(adminAudioServ *adminAudioServ, adminServ admin.Service) *AdminAudioHand {
	return &AdminAudioHand{adminAudioServ, adminServ}
}

func (h AdminAudioHand) AudioIndex(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			return h.adminServ.GetLessonName(c, lessonID)
		},
	}).ParseFiles(util.IncludeAdmin("audio")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["audios"] = h.GetAllAudioServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h AdminAudioHand) CheckOperations(c *gin.Context) {
	var status bool
	audioIDStr := c.Param("id")
	audioID, err := strconv.Atoi(audioIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	check := c.Param("status")
	if check == "true" {
		status = true
	} else {
		status = false
	}

	err = h.CheckOperationServ(c, audioID, status)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/admin/audio")

}
