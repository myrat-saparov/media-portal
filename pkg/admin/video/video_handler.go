package adminVideo

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"MediaPortal_2.0/pkg/admin"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type AdminVideoHand struct {
	Service
	adminServ admin.Service
}

func NewHandler(adminVideoServ *adminVideoServ, adminServ admin.Service) *AdminVideoHand {
	return &AdminVideoHand{adminVideoServ, adminServ}
}

func (h AdminVideoHand) VideoIndex(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			return h.adminServ.GetLessonName(c, lessonID)
		},
	}).ParseFiles(util.IncludeAdmin("video")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["videos"] = h.GetAllLessonsServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h AdminVideoHand) CheckOperations(c *gin.Context) {
	var check bool
	videoIDStr := c.Param("id")
	videoID, err := strconv.Atoi(videoIDStr)
	if err != nil {
		fmt.Println(err)
		return
	}
	status := c.Param("status")
	if status == "true" {
		check = true
	} else {
		check = false
	}

	err = h.CheckOperationServ(c, videoID, check)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(http.StatusSeeOther, "/admin/video")
}
