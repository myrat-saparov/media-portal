package adminVideo

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type adminVideoRepo struct {
	*gorm.DB
}

func NewRepository(dbConn *gorm.DB) *adminVideoRepo {
	return &adminVideoRepo{dbConn}
}

func (r adminVideoRepo) GetAllLessons(c *gin.Context) ([]Video, error) {
	var videos []Video

	query := "SELECT * FROM videos"

	result := r.DB.WithContext(c).Raw(query).Scan(&videos)
	if result.Error != nil {
		return nil, result.Error
	}

	return videos, nil
}

func (r adminVideoRepo) CheckOperation(c *gin.Context, videoID int, status bool) error {
	query := "UPDATE videos SET verification=$1, check_admin = $2 WHERE id = $3"

	result := r.DB.Exec(query, status, true, videoID)
	if result.Error != nil {
		return result.Error
	}
	return nil
}
