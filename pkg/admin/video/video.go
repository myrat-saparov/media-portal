package adminVideo

import (
	"github.com/gin-gonic/gin"
)

type Video struct {
	ID           int    `form:"-"`
	UserID       int    `form:"-"`
	Name         string `form:"Name"`
	Author       string `form:"author"`
	Description  string `form:"Description"`
	VideoURL     string `form:"-"`
	LessonID     int    `form:"LessonID"`
	Verification bool   `form:"-"`
	CheckAdmin   bool   `gorm:"check_admin"`
}

type Service interface {
	GetAllLessonsServ(c *gin.Context) []Video
	CheckOperationServ(c *gin.Context, videoID int, status bool) error
}

type Repository interface {
	GetAllLessons(c *gin.Context) ([]Video, error)
	CheckOperation(c *gin.Context, videoID int, status bool) error
}
