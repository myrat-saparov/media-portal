package adminVideo

import (
	"fmt"
	"sort"

	"github.com/gin-gonic/gin"
)

type adminVideoServ struct {
	Repository
}

func NewService(adminVideRepo *adminVideoRepo) *adminVideoServ {
	return &adminVideoServ{adminVideRepo}
}

func (s adminVideoServ) GetAllLessonsServ(c *gin.Context) []Video {
	videos, err := s.GetAllLessons(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	sort.Slice(videos, func(i, j int) bool {
		return !videos[i].CheckAdmin && videos[j].CheckAdmin
	})
	return videos
}

func (s adminVideoServ) CheckOperationServ(c *gin.Context, videoID int, status bool) error {
	return s.CheckOperation(c, videoID, status)

}
