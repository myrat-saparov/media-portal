package lesson

import (
	"errors"
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"MediaPortal_2.0/pkg/admin"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type HandLesson struct {
	ServLesson
	admin.Service
}

func NewHandler(service ServLesson, adminServ admin.Service) *HandLesson {
	return &HandLesson{service, adminServ}
}

func (h HandLesson) LessonIndex(c *gin.Context) {
	views, err := template.ParseFiles(util.IncludeAdmin("dashboard/lesson")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["lesson"] = h.GetAllLessonServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandLesson) AddLesson(c *gin.Context) {
	var lesson Lesson
	err := c.ShouldBind(&lesson)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = h.AddLessonServ(c, lesson)
	if err != nil {
		if errors.Is(err, errors.New("lesson name already exists")) {
			fmt.Println(err)
			http.Redirect(c.Writer, c.Request, "/admin/login", 303)
		} else {
			fmt.Println(err)
			return
		}
	}

	http.Redirect(c.Writer, c.Request, "/admin/lesson", 303)
}

func (h *HandLesson) DeleteLesson(c *gin.Context) {
	idStr := c.Param("id")

	id, err := strconv.Atoi(idStr)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = h.DeleteLessonServ(c, id)
	if err != nil {
		fmt.Println(err)
		return
	}

	http.Redirect(c.Writer, c.Request, "/admin/lesson", 303)
}
