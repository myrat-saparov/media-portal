package lesson

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

type service struct {
	RepoLesson
}

func NewService(repo RepoLesson) *service {
	return &service{repo}
}

func (s service) AddLessonServ(c *gin.Context, lesson Lesson) error {
	return s.AddLessonRepo(c, lesson)
}

func (s service) GetAllLessonServ(c *gin.Context) []Lesson {
	lessons, err := s.GetAllLessonRepo(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return lessons
}

func (s service) DeleteLessonServ(c *gin.Context, id int) error {
	return s.DeleteLessonRepo(c, id)
}
