package lesson

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) RepoLesson {
	return &repository{db}
}

func (r repository) AddLessonRepo(c *gin.Context, lesson Lesson) error {
	if lesson.LessonName == "" {
		return errors.New("lesson name is empty")
	}

	check := r.DB.Where("lesson_name = ?", lesson.LessonName).First(&Lesson{})
	if check.Error != nil {
		if errors.Is(check.Error, gorm.ErrRecordNotFound) {
			result := r.DB.WithContext(c).Create(&lesson)
			if result.Error != nil {
				return result.Error
			}
		} else {
			return check.Error
		}
	} else {
		return errors.New("lesson name already exists")
	}

	return nil
}

func (r repository) GetAllLessonRepo(c *gin.Context) ([]Lesson, error) {
	var lessons []Lesson

	query := "SELECT * FROM lessons"
	result := r.DB.WithContext(c).Raw(query).Scan(&lessons)
	if result.Error != nil {
		return nil, result.Error
	}

	return lessons, nil
}

func (r repository) DeleteLessonRepo(c *gin.Context, id int) error {
	query := "DELETE FROM lessons WHERE id = $1"

	result := r.DB.WithContext(c).Exec(query, id)
	if result.Error != nil {
		return result.Error
	}

	return nil
}
