package lesson

import "github.com/gin-gonic/gin"

type Lesson struct {
	ID         int    `json:"id"`
	LessonName string `form:"lessonName"`
}

type ServLesson interface {
	AddLessonServ(c *gin.Context, lesson Lesson) error
	GetAllLessonServ(c *gin.Context) []Lesson
	DeleteLessonServ(c *gin.Context, id int) error
}

type RepoLesson interface {
	AddLessonRepo(c *gin.Context, lesson Lesson) error
	GetAllLessonRepo(c *gin.Context) ([]Lesson, error)
	DeleteLessonRepo(c *gin.Context, id int) error
}
