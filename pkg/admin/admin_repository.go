package admin

import (
	"errors"

	"MediaPortal_2.0/pkg/user"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) Repository {
	return repository{db: db}
}

func (r repository) GetAdminByMail(ctx *gin.Context, mail string) (Admins, error) {
	var admin Admins
	query := "email = $1"

	result := r.db.WithContext(ctx).Where(query, mail).First(&admin)
	if result.Error != nil {
		if errors.Is(gorm.ErrRecordNotFound, result.Error) {
			return Admins{}, errors.New("admin not found")
		}
		return Admins{}, result.Error
	}

	return admin, nil
}

func (r repository) GetAllUsersRepository(c *gin.Context) ([]user.Users, error) {
	var users []user.Users

	query := "SELECT * FROM Users"
	result := r.db.WithContext(c).Raw(query).Scan(&users)
	if result.Error != nil {
		return users, result.Error
	}

	return users, nil
}

func (r repository) GetLessonByID(c *gin.Context, lessonID int) (Lesson, error) {
	var lesson Lesson

	query := "SELECT * FROM lessons WHERE id = ?"

	result := r.db.WithContext(c).Raw(query, lessonID).First(&lesson)
	if result.Error != nil {
		return lesson, result.Error
	}

	return lesson, nil
}

func (r repository) GetOSByID(c *gin.Context, osID int) (string, error) {
	var osName string

	query := "SELECT os_name FROM os WHERE id = ?"

	result := r.db.WithContext(c).Raw(query, osID).Scan(&osName)
	if result.Error != nil {
		return "", result.Error
	}

	return osName, nil
}
