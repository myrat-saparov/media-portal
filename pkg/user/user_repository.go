package user

import (
	"errors"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) GetUserByMail(c *gin.Context, mail string) (Users, error) {
	var user Users

	log.Printf("Fetching user with email: %s", mail)

	query := "SELECT * FROM users WHERE email=$1"

	result := r.DB.WithContext(c).Raw(query, mail).Scan(&user)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return user, errors.New("user not found")
		}
		return user, result.Error
	}

	log.Printf("Fetched user: %+v", user)

	return user, nil
}

func (r *repository) RegisterRepository(c *gin.Context, user Users) error {

	stmt := r.DB.WithContext(c).Create(&user)

	if stmt.Error != nil {
		return stmt.Error
	}

	return nil
}

func (r *repository) GetLessonName(c *gin.Context, LessonID int) (string, error) {
	var lesson string

	query := "SELECT lesson_name FROM lessons WHERE id = $1"

	result := r.DB.WithContext(c).Raw(query, LessonID).Scan(&lesson)
	if result.Error != nil {
		return "", result.Error
	}

	return lesson, nil
}

func (r *repository) GetOSName(c *gin.Context, LessonID int) (string, error) {
	var os string

	query := "SELECT os_name FROM os WHERE id = $1"

	result := r.DB.WithContext(c).Raw(query, LessonID).Scan(&os)
	if result.Error != nil {
		return "", result.Error
	}

	return os, nil
}
