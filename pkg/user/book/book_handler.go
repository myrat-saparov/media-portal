package book

import (
	"fmt"
	"html/template"
	"net/http"

	"MediaPortal_2.0/pkg/user"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type HandBook struct {
	ServBook
	user.Service
}

func NewHandler(service ServBook, adminServ user.Service) *HandBook {
	return &HandBook{service, adminServ}
}

func (h HandBook) BookIndex(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			lessonName := h.GetLessonNameServ(c, lessonID)
			return lessonName
		},
		"bookFormat": func(bookID int) string {
			return h.GetBookFormat(c, bookID)
		},
	}).ParseFiles(util.IncludeSite("book")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["book"] = h.GetAllBookServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandBook) BookSearch(c *gin.Context) {
	searchTerm := c.Request.FormValue("search")

	if searchTerm == "" {
		c.Redirect(http.StatusSeeOther, "/index/book")
	}

	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			lessonName := h.GetLessonNameServ(c, lessonID)
			return lessonName
		},
	}).ParseFiles(util.IncludeSite("book/search")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["book"] = h.SearchBookServ(c, searchTerm)
	data["searchTerm"] = searchTerm
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}
