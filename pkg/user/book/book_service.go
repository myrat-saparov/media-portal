package book

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

type service struct {
	RepoBook
}

func NewService(repo RepoBook) *service {
	return &service{repo}
}

func (s service) GetAllBookServ(c *gin.Context) []Book {
	books, err := s.GetAllBookRepo(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var filteredBooks []Book

	for _, book := range books {
		if !(!book.Verification && book.CheckAdmin) {
			filteredBooks = append(filteredBooks, book)
		}
	}

	return filteredBooks
}

func (s service) SearchBookServ(c *gin.Context, searchTerm string) []Book {
	books, err := s.SearchBookRepo(c, searchTerm)
	if err != nil {
		fmt.Println(err)
		return books
	}

	var filteredBooks []Book

	for _, book := range books {
		if !(!book.Verification && book.CheckAdmin) {
			filteredBooks = append(filteredBooks, book)
		}
	}

	return filteredBooks
}

func (s service) GetBookFormat(c *gin.Context, bookID int) string {
	book, err := s.GetBookByID(c, bookID)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	bookFormat := strings.LastIndex(book.BookURL, ".")
	if bookFormat == -1 {
		fmt.Println("format not found")
		return ""
	}

	return book.BookURL[bookFormat+1:]
}
