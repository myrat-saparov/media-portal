package book

import "github.com/gin-gonic/gin"

type Book struct {
	ID           int
	UserID       int
	Name         string
	RealeseDate  string
	PictureURL   string
	Size         float32
	Writer       string
	BookURL      string
	LessonID     int
	Verification bool
	CheckAdmin   bool
}

type ServBook interface {
	GetAllBookServ(c *gin.Context) []Book
	SearchBookServ(c *gin.Context, searchTerm string) []Book
	GetBookFormat(c *gin.Context, bookID int) string
}

type RepoBook interface {
	GetAllBookRepo(c *gin.Context) ([]Book, error)
	SearchBookRepo(c *gin.Context, searchTerm string) ([]Book, error)
	GetBookByID(c *gin.Context, bookID int) (Book, error)
}
