package book

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) RepoBook {
	return &repository{db}
}

func (r repository) GetAllBookRepo(c *gin.Context) ([]Book, error) {
	var books []Book

	query := "SELECT * FROM books"
	result := r.DB.WithContext(c).Raw(query).Scan(&books)
	if result.Error != nil {
		return nil, result.Error
	}

	return books, nil
}

func (r repository) SearchBookRepo(c *gin.Context, searchTerm string) ([]Book, error) {
	var books []Book

	searchTerm = "%" + searchTerm + "%"
	query := "SELECT * FROM books WHERE lower(name) LIKE lower($1)"
	result := r.DB.WithContext(c).Raw(query, searchTerm).Scan(&books)
	if result.Error != nil {
		return nil, result.Error
	}

	return books, nil
}

func (r repository) GetBookByID(c *gin.Context, bookID int) (Book, error) {
	var book Book

	query := "SELECT * FROM books WHERE id = $1"

	result := r.DB.WithContext(c).Raw(query, bookID).First(&book)
	if result.Error != nil {
		return book, result.Error
	}

	return book, nil
}
