package user

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"

	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

type Handler struct {
	Service
}

func NewHandler(service Service) *Handler {
	return &Handler{service}
}

func (h *Handler) LoginIndex(c *gin.Context) {
	views, err := template.ParseFiles(util.IncludeSite("login")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = views.ExecuteTemplate(c.Writer, "index", nil)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h *Handler) Login(c *gin.Context) {
	var user Users
	err := c.ShouldBind(&user)
	if err != nil {
		fmt.Println(err)
		return
	}

	_, err = h.LoginService(c, user)
	if err != nil {
		fmt.Println(err)
		http.Redirect(c.Writer, c.Request, "/login", http.StatusFound)
		return
	}

	http.Redirect(c.Writer, c.Request, "/index/", http.StatusSeeOther)
}

// func (h *Handler) LoginWidthGoogle(c *gin.Context) {
// 	s, err := util.GenerateString()
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, "could not generated random string")
// 		return
// 	}

// 	session := sessions.Default(c)
// 	session.Set("state", s)
// 	err = session.Save()
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, "could not set cookies")
// 	}
// 	authUrl, err := h.GetGoogleAuthUrl(c, s)
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, "could not signed in google")
// 		return
// 	}
// 	c.Redirect(http.StatusTemporaryRedirect, authUrl)
// }

// func (h *Handler) LoginGoogleCallBack(c *gin.Context) {
// 	// session := sessions.Default(c)
// 	// if session.Get("state") != c.Query("code") {
// 	// 	c.JSON(http.StatusBadGateway, "invalid state data")
// 	// 	return
// 	// }
// 	code := c.Query("code")
// 	body, _, err := h.HandleGoogleCallback(c, code)
// 	if err != nil {
// 		fmt.Println(err)
// 		c.Redirect(http.StatusTemporaryRedirect, "/login")
// 	}

// 	c.SetCookie("user", string(body), int(time.Now().Add(24*time.Hour).Unix()), "/", "localhost", true, false)

// 	userString := string(body)
// 	var newUser userAuth
// 	err = json.Unmarshal([]byte(userString), &newUser)
// 	if err != nil {
// 		fmt.Println(err)
// 		c.Redirect(http.StatusTemporaryRedirect, "/login")
// 	}

// 	user := Users{
// 		Name:  newUser.Nickname,
// 		Email: newUser.Email}

// 	_, err = h.LoginGoogleService(c, user)
// 	if err != nil {
// 		fmt.Println(err)

// 	}

// 	c.Redirect(http.StatusTemporaryRedirect, "/index/")
// }

func (h *Handler) Logout(c *gin.Context) {
	err := h.LogoutService(c)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(303, "/login")
}

func (h *Handler) UserIdentity(c *gin.Context) {
	cookie, err := c.Request.Cookie("user")
	if err != nil {
		fmt.Println(err)
		c.Redirect(http.StatusFound, "/login")
		return
	}

	if cookie == nil || cookie.Value == "" {
		c.Redirect(http.StatusFound, "/login")
		return
	}

	userId, err := h.ParseToken(cookie.Value)
	if err != nil {
		fmt.Println(err)
		http.Redirect(c.Writer, c.Request, "/login", http.StatusFound)
		return
	}

	c.Set("UserId", userId)
}

func (h *Handler) SignUpIndex(c *gin.Context) {
	views, err := template.ParseFiles(util.IncludeSite("signup")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = views.ExecuteTemplate(c.Writer, "index", nil)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h *Handler) SignUp(c *gin.Context) {
	var user Users
	err := c.ShouldBind(&user)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = h.RegisterService(c, user)
	if err != nil {
		fmt.Println(err)
		return
	}

	c.Redirect(302, "/index/")
}

func (h *Handler) Index(c *gin.Context) {
	views, err := template.ParseFiles(util.IncludeSite("index")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = views.ExecuteTemplate(c.Writer, "index", nil)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func DecodeJwt(tokenString string) (Users, error) {
	var newUser Users

	token, err := jwt.ParseWithClaims(tokenString, &jwt.MapClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(Key), nil
	})
	if err != nil {
		return newUser, err
	}

	if claims, ok := token.Claims.(*jwt.MapClaims); ok && token.Valid {
		userData, err := json.Marshal(claims)
		if err != nil {
			return newUser, err
		}
		err = json.Unmarshal(userData, &newUser)
		if err != nil {
			return newUser, err
		}
	} else {
		return newUser, fmt.Errorf("invalid token")
	}

	return newUser, nil
}
