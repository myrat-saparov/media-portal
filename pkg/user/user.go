package user

import (
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

type Users struct {
	ID       int    `gorm:"column:id"`
	Name     string `gorm:"column:name" form:"username" json:"username"`
	Email    string `gorm:"column:email" form:"email" json:"email"`
	Phone    string `gorm:"column:phone" form:"phone" json:"phone"`
	Password string `gorm:"column:password" form:"password" json:"password"`
}

func (Users) TableName() string {
	return "users"
}

// type userAuth struct {
// 	Sub           string    `json:"sub"`
// 	GivenName     string    `json:"given_name"`
// 	FamilyName    string    `json:"family_name"`
// 	Nickname      string    `json:"nickname"`
// 	Name          string    `json:"name"`
// 	Picture       string    `json:"picture"`
// 	Locale        string    `json:"locale"`
// 	UpdatedAt     time.Time `json:"updated_at"`
// 	Email         string    `json:"email"`
// 	EmailVerified bool      `json:"email_verified"`
// }

type JWTClaims struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
	jwt.RegisteredClaims
}

const Key = "27072004"

// const maxAge = 84600 * 30
// const isProd = false

type Service interface {
	LoginService(c *gin.Context, user Users) (string, error)
	// GetGoogleAuthUrl(c *gin.Context, randomString string) (string, error)
	// HandleGoogleCallback(c *gin.Context, code string) ([]byte, string, error)
	// LoginGoogleService(c *gin.Context, user Users) (string, error)
	LogoutService(c *gin.Context) error
	RegisterService(c *gin.Context, user Users) error
	ParseToken(accessToken string) (int, error)
	CreateToken(c *gin.Context, newUser Users) (string, error)
	GetLessonNameServ(c *gin.Context, LessonID int) string
	GetOSNameServ(c *gin.Context, OSID int) string
	GetUserByMailServ(c *gin.Context, mail string) (Users, error)
}

type Repository interface {
	GetUserByMail(c *gin.Context, mail string) (Users, error)
	RegisterRepository(c *gin.Context, newUser Users) error
	GetLessonName(c *gin.Context, LessonID int) (string, error)
	GetOSName(c *gin.Context, LessonID int) (string, error)
}
