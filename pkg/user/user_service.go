package user

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

type service struct {
	Repository
	// oauth2Config *oauth2.Config
}

func NewService(repo Repository) *service {
	// oauth2Config, err := NewAuth()
	// if err != nil {
	// 	fmt.Println(err)
	// 	return nil
	// }
	// return &service{repo, oauth2Config}
	return &service{repo}
}

// func NewAuth() (*oauth2.Config, error) {
// 	providerURL := fmt.Sprintf("https://" + os.Getenv("AUTH0_DOMAIN") + "/")
// 	provider, err := oidc.NewProvider(context.Background(), providerURL)
// 	if err != nil {
// 		return nil, fmt.Errorf("could not set provider: %v", err)
// 	}

// 	oauth2 := oauth2.Config{
// 		ClientID:     os.Getenv("AUTH0_CLIENT_ID"),
// 		ClientSecret: os.Getenv("AUTH0_CLIENT_SECRET"),
// 		RedirectURL:  os.Getenv("AUTH0_REDIRECT_URL"),
// 		Endpoint:     provider.Endpoint(),
// 		Scopes:       []string{oidc.ScopeOpenID, "profile", "email", "photo"},
// 	}

// 	return &oauth2, nil
// }

func (s *service) LoginService(c *gin.Context, newUser Users) (string, error) {
	user, err := s.GetUserByMail(c, newUser.Email)
	if err != nil {
		return "", err
	}

	if user.Password == "" {
		return "", errors.New("user not found")
	}

	if user.Password != newUser.Password {
		return "", errors.New("password incorrect")
	}

	token, err := s.CreateToken(c, user)
	if err != nil {
		return "", err
	}

	return token, nil
}

// func (s *service) GetGoogleAuthUrl(c *gin.Context, randomString string) (string, error) {
// 	authURL := s.oauth2Config.AuthCodeURL(randomString)
// 	return authURL, nil
// }

// func (s *service) HandleGoogleCallback(c *gin.Context, code string) ([]byte, string, error) {
// 	token, err := s.oauth2Config.Exchange(c, code)
// 	if err != nil {
// 		return nil, "", err
// 	}

// 	if !token.Valid() {
// 		return nil, "", errors.New("invalid auth token")
// 	}

// 	client := s.oauth2Config.Client(c, token)
// 	resp, err := client.Get("https://" + os.Getenv("AUTH0_DOMAIN") + "/userinfo")
// 	if err != nil {
// 		return nil, "", err
// 	}

// 	body, err := io.ReadAll(resp.Body)
// 	if err != nil {
// 		return nil, "", err
// 	}

// 	return body, token.AccessToken, nil
// }

// func (s *service) LoginGoogleService(c *gin.Context, user Users) (string, error) {
// 	user, err := s.GetUserByMail(c, user.Email)
// 	if err != nil {
// 		return "", err
// 	}

// 	if user.Password == "" {
// 		return "", errors.New("user not found")
// 	}

// 	token, err := s.CreateToken(c, user)
// 	if err != nil {
// 		return "", err
// 	}

// 	return token, nil
// }

func (s *service) LogoutService(c *gin.Context) error {
	http.SetCookie(c.Writer, &http.Cookie{
		Name:     "user",
		Value:    "",
		MaxAge:   -1,
		Path:     "/",
		HttpOnly: true,
	})

	return nil
}

func (s *service) RegisterService(c *gin.Context, newUser Users) error {
	//_, err := s.GetUserByMail(c, newUser.Email)
	//if err != nil {
	//	if !errors.Is(err, gorm.ErrRecordNotFound) {
	//		return err
	//	}
	err := s.RegisterRepository(c, newUser)
	if err != nil {
		return err
	}
	fmt.Println("register new user")
	//}

	_, err = s.CreateToken(c, newUser)
	if err != nil {
		return err
	}

	return nil
}

func (s *service) CreateToken(c *gin.Context, newUser Users) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, JWTClaims{
		Id:    newUser.ID,
		Name:  newUser.Name,
		Email: newUser.Email,
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    strconv.Itoa(newUser.ID),
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 24)),
		},
	})

	tokenString, err := token.SignedString([]byte(Key))
	if err != nil {
		return "", err
	}

	cookie := &http.Cookie{
		Name:    "user",
		Value:   tokenString,
		Expires: time.Now().Add(time.Hour * 24),
		Path:    "/",
	}

	http.SetCookie(c.Writer, cookie)

	return tokenString, nil
}

func (s *service) ParseToken(accessToken string) (int, error) {
	token, err := jwt.ParseWithClaims(accessToken, &JWTClaims{}, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(Key), nil
	})

	if err != nil {
		return 0, err
	}

	claims, ok := token.Claims.(*JWTClaims)
	if !ok {
		return 0, errors.New("invalid token")
	}

	return claims.Id, nil
}

func (s *service) GetLessonNameServ(c *gin.Context, LessonID int) string {
	lessonName, err := s.GetLessonName(c, LessonID)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	return lessonName
}

func (s *service) GetOSNameServ(c *gin.Context, OSID int) string {
	osName, err := s.GetOSName(c, OSID)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	return osName
}

func (s *service) GetUserByMailServ(c *gin.Context, mail string) (Users, error) {
	return s.GetUserByMail(c, mail)
}
