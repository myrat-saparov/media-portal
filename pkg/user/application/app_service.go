package app

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

type service struct {
	RepoApp
}

func NewService(repo RepoApp) *service {
	return &service{repo}
}

func (s service) GetAllAppServ(c *gin.Context) []App {
	apps, err := s.GetAllAppRepo(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var filteredApps []App

	for _, app := range apps {
		if !(!app.Verification && app.CheckAdmin) {
			filteredApps = append(filteredApps, app)
		}
	}

	return filteredApps
}

func (s service) SearchAppServ(c *gin.Context, searchTerm string) []App {
	apps, err := s.SearchAppRepo(c, searchTerm)
	if err != nil {
		fmt.Println(err)
		return apps
	}

	var filteredApps []App

	for _, app := range apps {
		if !(!app.Verification && app.CheckAdmin) {
			filteredApps = append(filteredApps, app)
		}
	}

	return filteredApps
}

func (s service) GetAppFormat(c *gin.Context, appID int) string {
	app, err := s.GetAppByID(c, appID)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	fmt.Println(app.AppURL, appID)
	appFormat := strings.LastIndex(app.AppURL, ".")
	if appFormat == -1 {
		fmt.Println("format not found")
		return ""
	}

	return app.AppURL[appFormat+1:]

}
