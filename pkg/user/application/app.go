package app

import "github.com/gin-gonic/gin"

type App struct {
	ID           int     `form:"id"`
	UserID       int     `form:"userID"`
	Name         string  `form:"name"`
	ReleaseDate  string  `form:"releaseDate"`
	PictureURL   string  `form:"pictureURL"`
	Size         float32 `form:"size"`
	AppURL       string  `form:"appURL"`
	OSID         int     `form:"osID"`
	Verification bool    `form:"verification"`
	CheckAdmin   bool
}

type ServApp interface {
	GetAllAppServ(c *gin.Context) []App
	SearchAppServ(c *gin.Context, searchTerm string) []App
	GetAppFormat(c *gin.Context, appID int) string
}

type RepoApp interface {
	GetAllAppRepo(c *gin.Context) ([]App, error)
	SearchAppRepo(c *gin.Context, searchTerm string) ([]App, error)
	GetAppByID(c *gin.Context, appID int) (App, error)
}
