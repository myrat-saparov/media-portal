package app

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) RepoApp {
	return &repository{db}
}

func (r repository) GetAllAppRepo(c *gin.Context) ([]App, error) {
	var apps []App

	query := "SELECT * FROM apps"
	result := r.DB.WithContext(c).Raw(query).Scan(&apps)
	if result.Error != nil {
		return nil, result.Error
	}

	return apps, nil
}

func (r repository) SearchAppRepo(c *gin.Context, searchTerm string) ([]App, error) {
	var apps []App

	searchTerm = "%" + searchTerm + "%"
	query := "SELECT * FROM apps WHERE lower(name) LIKE lower($1)"
	result := r.DB.WithContext(c).Raw(query, searchTerm).Scan(&apps)
	if result.Error != nil {
		return nil, result.Error
	}

	return apps, nil
}

func (r repository) GetAppByID(c *gin.Context, appID int) (App, error) {
	var app App

	query := "SELECT * FROM apps WHERE id=$1"

	result := r.DB.WithContext(c).Raw(query, appID).First(&app)
	if result.Error != nil {
		return app, result.Error
	}

	return app, nil
}
