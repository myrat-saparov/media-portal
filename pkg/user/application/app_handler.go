package app

import (
	"fmt"
	"html/template"
	"net/http"

	"MediaPortal_2.0/pkg/user"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type HandApp struct {
	ServApp
	user.Service
}

func NewHandler(service ServApp, adminServ user.Service) *HandApp {
	return &HandApp{service, adminServ}
}

func (h HandApp) AppIndex(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"osName": func(osID int) string {
			return h.GetOSNameServ(c, osID)
		},
		"appFormat": func(appID int) string {
			return h.GetAppFormat(c, appID)
		},
	}).ParseFiles(util.IncludeSite("application")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["app"] = h.GetAllAppServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandApp) AppSearch(c *gin.Context) {
	searchTerm := c.Request.FormValue("search")

	if searchTerm == "" {
		c.Redirect(http.StatusSeeOther, "/index/application")
	}

	views, err := template.ParseFiles(util.IncludeSite("application/search")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["app"] = h.SearchAppServ(c, searchTerm)
	data["searchTerm"] = searchTerm
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}
