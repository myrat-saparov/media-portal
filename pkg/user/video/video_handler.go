package video

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"MediaPortal_2.0/pkg/user"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type HandVideo struct {
	ServVideo
	user.Service
}

func NewHandler(service ServVideo, adminServ user.Service) *HandVideo {
	return &HandVideo{service, adminServ}
}

func (h HandVideo) VideoIndex(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			lessonName := h.GetLessonNameServ(c, lessonID)
			return lessonName
		},
	}).ParseFiles(util.IncludeSite("video")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["videos"] = h.GetAllVideoServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandVideo) VideoDetails(c *gin.Context) {
	views, err := template.ParseFiles(util.IncludeSite("video/detail")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	idString := c.Param("id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = h.VideoViewServ(c, id)
	if err != nil {
		fmt.Println(err)
	}

	data := make(map[string]interface{})
	data["video"] = h.GetAllVideoServ(c)
	data["selectedVideo"] = h.GetVideoByIDServ(c, id)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandVideo) VideoSearch(c *gin.Context) {
	searchTerm := c.Request.FormValue("search")

	if searchTerm == "" {
		c.Redirect(http.StatusSeeOther, "/index/video")
	}

	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			return h.GetLessonNameServ(c, lessonID)
		},
	}).ParseFiles(util.IncludeSite("video/search")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["videos"] = h.SearchVideoServ(c, searchTerm)
	data["searchTerm"] = searchTerm
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandVideo) VideoLike(c *gin.Context) {
	var like bool = false
	videoIDString := c.Param("id")
	videoID, err := strconv.Atoi(videoIDString)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid video ID"})
		return
	}

	token, err := c.Cookie("user")
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	userMail, err := user.DecodeJwt(token)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	user, err := h.GetUserByMailServ(c, userMail.Email)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "User not found"})
		return
	}

	isLike := c.Param("like")
	if isLike == "like" {
		like = true
	}

	err = h.VideoLikeServ(c, user.ID, videoID, like)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Could not process like/dislike"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true})
}
