package video

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

type service struct {
	RepoVideo
}

func NewService(repo RepoVideo) *service {
	return &service{repo}
}

func (s service) GetAllVideoServ(c *gin.Context) []Video {
	videos, err := s.GetAllVideoRepo(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var filteredVideos []Video

	for _, video := range videos {
		if !(!video.Verification && video.CheckAdmin) {
			filteredVideos = append(filteredVideos, video)
		}
	}

	return filteredVideos
}

func (s service) GetVideoByIDServ(c *gin.Context, id int) Video {
	video, err := s.GetVideoByID(c, id)
	if err != nil {
		fmt.Println(err)
		return video
	}

	videoLikes, err := s.GetAllLike(c, id)
	if err != nil {
		fmt.Println(err)
		return video
	}

	videoDislikes, err := s.GetAllDislike(c, id)
	if err != nil {
		fmt.Println(err)
		return video
	}

	video.Likes = len(videoLikes)
	video.Dislikes = len(videoDislikes)

	return video
}

func (s service) SearchVideoServ(c *gin.Context, searchTerm string) []Video {
	videos, err := s.SearchVideoRepo(c, searchTerm)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var filteredVideos []Video

	for _, video := range videos {
		if !(!video.Verification && video.CheckAdmin) {
			filteredVideos = append(filteredVideos, video)
		}
	}

	return filteredVideos
}

func (s service) VideoViewServ(c *gin.Context, videoID int) error {
	video, err := s.GetVideoByID(c, videoID)
	if err != nil {
		return err
	}

	views := video.CoutViews + 1

	err = s.VideoView(c, videoID, views)
	if err != nil {
		return err
	}

	return nil
}

func (s service) VideoLikeServ(c *gin.Context, userID, videoID int, like bool) error {
	err := s.VideoLike(c, userID, videoID, like)
	if err != nil {
		return err
	}
	return nil
}
