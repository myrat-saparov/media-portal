package video

import (
	"github.com/gin-gonic/gin"
)

type Video struct {
	ID           int
	UserID       int
	Name         string
	Description  string
	Author       string
	VideoURL     string
	LessonID     int
	RealeseDate  int
	Verification bool
	CheckAdmin   bool
	CoutViews    int
	Likes        int
	Dislikes     int
}

type VideoLike struct {
	ID      int `gorm:"primary_key"`
	UserID  int
	VideoID int
	IsLike  bool
}

type ServVideo interface {
	GetAllVideoServ(c *gin.Context) []Video
	GetVideoByIDServ(c *gin.Context, id int) Video
	SearchVideoServ(c *gin.Context, searchTerm string) []Video
	VideoViewServ(c *gin.Context, videoID int) error
	VideoLikeServ(c *gin.Context, userID, videoID int, like bool) error
}

type RepoVideo interface {
	GetAllVideoRepo(c *gin.Context) ([]Video, error)
	GetVideoByID(c *gin.Context, id int) (Video, error)
	SearchVideoRepo(c *gin.Context, searchTerm string) ([]Video, error)
	VideoView(c *gin.Context, videoID, views int) error
	VideoLike(c *gin.Context, userID int, videoID int, like bool) error
	GetAllLike(c *gin.Context, videoID int) ([]VideoLike, error)
	GetAllDislike(c *gin.Context, videoID int) ([]VideoLike, error)
}
