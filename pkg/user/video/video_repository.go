package video

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/lib/pq"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) RepoVideo {
	return &repository{db}
}

func (r repository) GetAllVideoRepo(c *gin.Context) ([]Video, error) {
	var videos []Video

	query := "SELECT * FROM videos"
	result := r.DB.WithContext(c).Raw(query).Scan(&videos)
	if result.Error != nil {
		return nil, result.Error
	}

	return videos, nil
}

func (r repository) GetVideoByID(c *gin.Context, id int) (Video, error) {
	var video Video

	query := "SELECT * FROM VIDEOS WHERE id = $1"

	result := r.DB.WithContext(c).Raw(query, id).Scan(&video)
	if result.Error != nil {
		return video, result.Error
	}

	return video, nil
}

func (r repository) SearchVideoRepo(c *gin.Context, searchTerm string) ([]Video, error) {
	var video []Video

	searchTerm = "%" + searchTerm + "%"
	query := "SELECT * FROM videos WHERE lower(name) LIKE lower($1)"

	result := r.DB.WithContext(c).Raw(query, searchTerm).Scan(&video)
	if result.Error != nil {
		return video, result.Error
	}

	return video, nil
}

func (r repository) VideoView(c *gin.Context, videoID int, views int) error {
	query := "UPDATE videos SET cout_views=$1 WHERE id=$2"

	result := r.DB.Exec(query, views, videoID)

	return result.Error
}

func (r repository) VideoLike(c *gin.Context, userID, videoID int, like bool) error {
	query := "INSERT INTO videolikes (user_id, video_id, is_like) VALUES (?, ?, ?) ON CONFLICT (user_id, video_id) DO UPDATE SET is_like = EXCLUDED.is_like"

	result := r.DB.Exec(query, userID, videoID, like)

	if result.Error != nil {
		if pqErr, ok := result.Error.(*pq.Error); ok {
			if pqErr.Code == "23505" {
				fmt.Println("Duplicate key value violation: A similar entry already exists.")

				updateResult := r.DB.Model(&VideoLike{}).Where("user_id = ? AND video_id = ?", userID, videoID).Update("is_like", like)
				if updateResult.Error != nil {
					return fmt.Errorf("error updating like after duplicate key error: %v", updateResult.Error)
				}
			} else {
				return fmt.Errorf("error executing query: %v", pqErr)
			}
		} else {
			return fmt.Errorf("error executing query: %v", result.Error)
		}
	}

	return nil
}

func (r repository) GetAllLike(c *gin.Context, videoID int) ([]VideoLike, error) {
	var videoLikes []VideoLike
	query := "SELECT * FROM videolikes WHERE video_id = $1 AND is_like = true"

	result := r.DB.WithContext(c).Raw(query, videoID).Scan(&videoLikes)
	if result.Error != nil {
		return nil, result.Error
	}

	return videoLikes, nil
}

func (r repository) GetAllDislike(c *gin.Context, videoID int) ([]VideoLike, error) {
	var videoLikes []VideoLike
	query := "SELECT * FROM videolikes WHERE video_id = $1 AND is_like = false"

	result := r.DB.WithContext(c).Raw(query, videoID).Scan(&videoLikes)
	if result.Error != nil {
		return nil, result.Error
	}

	return videoLikes, nil
}
