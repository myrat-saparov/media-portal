package music

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

type service struct {
	RepoMusic
}

func NewService(repo RepoMusic) *service {
	return &service{repo}
}

func (s service) GetAllMusicServ(c *gin.Context) []Music {
	musics, err := s.GetAllMusicRepo(c)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var filteredAudios []Music

	for _, audio := range musics {
		if !(!audio.Verification && audio.CheckAdmin) {
			filteredAudios = append(filteredAudios, audio)
		}
	}

	return filteredAudios
}

func (s service) SearchMusicServ(c *gin.Context, searchTerm string) []Music {
	musics, err := s.SearchMusicRepo(c, searchTerm)
	if err != nil {
		fmt.Println(err)
		return musics
	}

	var filteredAudios []Music

	for _, audio := range musics {
		if !(!audio.Verification && audio.CheckAdmin) {
			filteredAudios = append(filteredAudios, audio)
		}
	}

	return filteredAudios
}
