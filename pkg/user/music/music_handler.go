package music

import (
	"fmt"
	"html/template"
	"net/http"

	"MediaPortal_2.0/pkg/user"
	"MediaPortal_2.0/util"
	"github.com/gin-gonic/gin"
)

type HandMusic struct {
	ServMusic
	user.Service
}

func NewHandler(service ServMusic, adminServ user.Service) *HandMusic {
	return &HandMusic{service, adminServ}
}

func (h HandMusic) MusicIndex(c *gin.Context) {
	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			lessonName := h.GetLessonNameServ(c, lessonID)
			return lessonName
		},
	}).ParseFiles(util.IncludeSite("audio")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["audios"] = h.GetAllMusicServ(c)
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (h HandMusic) MusicSearch(c *gin.Context) {
	searchTerm := c.Request.FormValue("search")

	if searchTerm == "" {
		c.Redirect(http.StatusSeeOther, "/index/audio")
	}

	views, err := template.New("index").Funcs(template.FuncMap{
		"lessonName": func(lessonID int) string {
			lessonName := h.GetLessonNameServ(c, lessonID)
			return lessonName
		},
	}).ParseFiles(util.IncludeSite("audio/search")...)
	if err != nil {
		fmt.Println(err)
		return
	}

	data := make(map[string]interface{})
	data["audios"] = h.SearchMusicServ(c, searchTerm)
	data["searchTerm"] = searchTerm
	err = views.ExecuteTemplate(c.Writer, "index", data)
	if err != nil {
		fmt.Println(err)
		return
	}
}
