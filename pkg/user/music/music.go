package music

import "github.com/gin-gonic/gin"

type Music struct {
	ID           int
	UserID       int
	Name         string `form:"name"`
	Singer       string `form:"singer"`
	AudioURL     string
	LessonID     int  `form:"lessonID"`
	Verification bool `form:"verification"`
	CheckAdmin   bool
}

type ServMusic interface {
	GetAllMusicServ(c *gin.Context) []Music
	SearchMusicServ(c *gin.Context, searchTerm string) []Music
}

type RepoMusic interface {
	GetAllMusicRepo(c *gin.Context) ([]Music, error)
	SearchMusicRepo(c *gin.Context, searchTerm string) ([]Music, error)
}
