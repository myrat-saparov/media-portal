package music

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) RepoMusic {
	return &repository{db}
}

func (r repository) GetAllMusicRepo(c *gin.Context) ([]Music, error) {
	var musics []Music

	query := "SELECT * FROM audios"
	result := r.DB.WithContext(c).Raw(query).Scan(&musics)
	if result.Error != nil {
		return nil, result.Error
	}

	return musics, nil
}

func (r repository) SearchMusicRepo(c *gin.Context, searchTerm string) ([]Music, error) {
	var musics []Music

	searchTerm = "%" + searchTerm + "%"
	query := "SELECT * FROM audios WHERE lower(name) LIKE lower($1)"
	result := r.DB.WithContext(c).Raw(query, searchTerm).Scan(&musics)
	if result.Error != nil {
		return nil, result.Error
	}

	return musics, nil
}
