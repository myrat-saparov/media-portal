package util

import (
	"crypto/sha256"
	"fmt"
)

func CreatePassword(password string) string {
	return fmt.Sprintf("%x", sha256.Sum256([]byte(password)))
}
