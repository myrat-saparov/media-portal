package util

import (
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

func UploadFile(c *gin.Context, folderPath string, formName string) (string, os.FileInfo, error) {
	// Get File in Form
	file, header, err := c.Request.FormFile(formName)
	if err != nil {
		return "", nil, err
	}

	// Create FileName
	fileName := strconv.FormatInt(time.Now().UnixNano(), 10) + "_" + header.Filename
	filePath := filepath.Join("./uploads/", folderPath, fileName)

	// Convert the file path to use forward slashes
	filePath = strings.ReplaceAll(filePath, "\\", "/")

	// Create directories if they don't exist
	err = os.MkdirAll(filepath.Dir(filePath), 0755)
	if err != nil {
		return "", nil, err
	}

	// Create File
	openFile, err := os.OpenFile("./../"+filePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return "", nil, err
	}
	defer openFile.Close()

	// Copy file
	_, err = io.Copy(openFile, file)
	if err != nil {
		return "", nil, err
	}

	fileInfo, err := openFile.Stat()
	if err != nil {
		return "", nil, err
	}

	return filePath, fileInfo, nil
}
