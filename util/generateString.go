package util

import (
	"crypto/rand"
	"encoding/base64"
)

func GenerateString() (string, error) {
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}

	randomS := base64.StdEncoding.EncodeToString(b)
	return randomS, nil
}
