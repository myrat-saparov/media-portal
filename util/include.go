package util

import (
	"fmt"
	"path/filepath"
)

func IncludeAdmin(path string) []string {
	files, err := filepath.Glob("../front/admin/template/*.html")
	if err != nil {
		fmt.Println(err)
		return nil
	}

	file, err := filepath.Glob("../front/admin/" + path + "/*.html")
	if err != nil {
		fmt.Println(err)
		return nil
	}
	for _, f := range file {
		files = append(files, f)
	}

	return files
}

func IncludeSite(path string) []string {
	files, err := filepath.Glob("../front/site/template/*.html")
	if err != nil {
		fmt.Println(err)
		return nil
	}

	file, err := filepath.Glob("../front/site/" + path + "/*.html")
	if err != nil {
		fmt.Println(err)
		return nil
	}
	for _, f := range file {
		files = append(files, f)
	}

	return files
}

func IncludeProfile(path string) []string {
	files, err := filepath.Glob("../front/profile/template/*.html")
	if err != nil {
		fmt.Println(err)
		return nil
	}

	file, err := filepath.Glob("../front/profile/" + path + "/*.html")
	if err != nil {
		fmt.Println(err)
		return nil
	}
	for _, f := range file {
		files = append(files, f)
	}

	return files
}
