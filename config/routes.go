package config

import (
	"MediaPortal_2.0/pkg/admin"
	adminApp "MediaPortal_2.0/pkg/admin/application"
	adminAudio "MediaPortal_2.0/pkg/admin/audio"
	adminBook "MediaPortal_2.0/pkg/admin/book"
	"MediaPortal_2.0/pkg/admin/lesson"
	"MediaPortal_2.0/pkg/admin/os"
	adminVideo "MediaPortal_2.0/pkg/admin/video"
	"MediaPortal_2.0/pkg/profile"
	appProf "MediaPortal_2.0/pkg/profile/application"
	bookProf "MediaPortal_2.0/pkg/profile/book"
	musicProf "MediaPortal_2.0/pkg/profile/music"
	videoProf "MediaPortal_2.0/pkg/profile/video"
	"MediaPortal_2.0/pkg/user"
	app "MediaPortal_2.0/pkg/user/application"
	"MediaPortal_2.0/pkg/user/book"
	"MediaPortal_2.0/pkg/user/music"
	"MediaPortal_2.0/pkg/user/video"
	"github.com/gin-gonic/gin"
)

// var store = cookie.NewStore([]byte(user.Key))

func InitRouter(adminHand *admin.Handler, lessonHand *lesson.HandLesson, osHand *os.HandOS, adminVideoHand *adminVideo.AdminVideoHand, adminBookHand *adminBook.AdminBookHand, adminAppHand *adminApp.AdminAppHand, adminAudioHand *adminAudio.AdminAudioHand, siteHand *user.Handler, bookHand *book.HandBook, appHand *app.HandApp, musicHand *music.HandMusic, videoHand *video.HandVideo, profHand *profile.Handler, profAppHand *appProf.HandApp, profBookHand *bookProf.HandBook, profMusicHand *musicProf.HandMusic, profVideoHand *videoProf.HandVideo) *gin.Engine {
	r := gin.Default()

	// r.Use(sessions.Sessions("auth-sessions", store))

	admin := r.Group("/admin")
	{
		admin.GET("/", adminHand.Check, adminHand.Dashboard)
		admin.GET("/login", adminHand.LoginIndex)
		admin.POST("/login", adminHand.Login)
		admin.GET("/logout", adminHand.Check, adminHand.Logout)

		lesson := admin.Group("/lesson", adminHand.Check)
		{
			lesson.GET("/", lessonHand.LessonIndex)
			lesson.POST("/add", lessonHand.AddLesson)
			lesson.GET("/delete/:id", lessonHand.DeleteLesson)
		}

		os := admin.Group("/os", adminHand.Check)
		{
			os.GET("/", osHand.OSIndex)
			os.POST("/add", osHand.AddOS)
			os.GET("/delete/:id", osHand.DeleteOS)
		}

		video := admin.Group("/video", adminHand.Check)
		{
			video.GET("/", adminVideoHand.VideoIndex)
			video.GET("/verify/:status/:id", adminVideoHand.CheckOperations)
		}

		audio := admin.Group("/audio", adminHand.Check)
		{
			audio.GET("/", adminAudioHand.AudioIndex)
			audio.GET("/verify/:status/:id", adminAudioHand.CheckOperations)
		}

		app := admin.Group("/application", adminHand.Check)
		{
			app.GET("/", adminAppHand.AppIndex)
			app.GET("/verify/:status/:id", adminAppHand.CheckOperations)
		}

		book := admin.Group("/book", adminHand.Check)
		{
			book.GET("/", adminBookHand.BookIndex)
			book.GET("/verify/:status/:id", adminBookHand.CheckOperations)
		}
	}

	r.GET("/login", siteHand.LoginIndex)
	r.POST("/login", siteHand.Login)
	r.GET("/logout", siteHand.Logout)
	r.GET("/signup", siteHand.SignUpIndex)
	r.POST("/signup", siteHand.SignUp)
	// r.GET("/login_google", siteHand.LoginWidthGoogle)
	// r.GET("/callback", siteHand.LoginGoogleCallBack)
	// r.GET("/signin_google", siteHand.LoginWidthGoogle)
	// r.GET("/register_callback")
	// r.POST("/signup_google")

	index := r.Group("/index", siteHand.UserIdentity)
	{
		index.GET("/", siteHand.Index)
		index.GET("/book", bookHand.BookIndex)
		index.POST("/bookSearch", bookHand.BookSearch)
		index.GET("/application", appHand.AppIndex)
		index.POST("/appSearch", appHand.AppSearch)
		index.GET("/audio", musicHand.MusicIndex)
		index.POST("/audioSearch", musicHand.MusicSearch)
		index.GET("/video", videoHand.VideoIndex)
		index.GET("/video/:id", videoHand.VideoDetails)
		index.GET("/video/:id/:like", videoHand.VideoLike)
		index.POST("/videoSearch", videoHand.VideoSearch)
	}

	profile := r.Group("/profile", siteHand.UserIdentity)
	{
		profile.GET("/", profHand.Index)

		book := profile.Group("/book")
		{
			book.GET("/", profBookHand.BookIndex)
			book.GET("/add", profBookHand.BookAdd)
			book.POST("/add", profBookHand.AddNewBook)
			book.GET("/edit/:id", profBookHand.CheckUser("edit"), profBookHand.EditBook)
			book.POST("/edit/:id", profBookHand.CheckUser("update"), profBookHand.UpdateBook)
			book.GET("/delete/:id", profBookHand.CheckUser("delete"), profBookHand.DeleteBook)
		}

		application := profile.Group("/application")
		{
			application.GET("/", profAppHand.AppIndex)
			application.GET("/add", profAppHand.AppAdd)
			application.POST("/add", profAppHand.AddNewApp)
			application.GET("/edit/:id", profAppHand.CheckUser("edit"), profAppHand.EditApp)
			application.POST("/edit/:id", profAppHand.CheckUser("update"), profAppHand.UpdateApp)
			application.GET("/delete/:id", profAppHand.CheckUser("delete"), profAppHand.DeleteApp)
		}

		audio := profile.Group("/audio")
		{
			audio.GET("/", profMusicHand.MusicIndex)
			audio.GET("/add", profMusicHand.AudioAdd)
			audio.POST("/add", profMusicHand.AddNewAudio)
			audio.GET("/edit/:id", profMusicHand.CheckUser("edit"), profMusicHand.EditAudio)
			audio.POST("/edit/:id", profMusicHand.CheckUser("update"), profMusicHand.UpdateAudio)
			audio.GET("/delete/:id", profMusicHand.CheckUser("delete"), profMusicHand.DeleteAudio)
		}

		video := profile.Group("/video")
		{
			video.GET("/", profVideoHand.VideoIndex)
			video.GET("/add", profVideoHand.VideoAdd)
			video.POST("/add", profVideoHand.AddNewVideo)
			video.GET("/edit/:id", profVideoHand.CheckUser("edit"), profVideoHand.EditVideo)
			video.POST("/edit/:id", profVideoHand.CheckUser("update"), profVideoHand.UpdateVideo)
			video.GET("/delete/:id", profVideoHand.CheckUser("delete"), profVideoHand.DeleteVideo)
		}

	}

	r.NoRoute()

	r.Static("/front", "./../front")
	r.Static("/uploads", "./../uploads")

	return r
}
