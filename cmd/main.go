package main

import (
	"fmt"
	"log"
	"net/http"

	"MediaPortal_2.0/config"
	"MediaPortal_2.0/db"
	"MediaPortal_2.0/pkg/admin"
	adminApp "MediaPortal_2.0/pkg/admin/application"
	adminAudio "MediaPortal_2.0/pkg/admin/audio"
	adminBook "MediaPortal_2.0/pkg/admin/book"
	"MediaPortal_2.0/pkg/admin/lesson"
	"MediaPortal_2.0/pkg/admin/os"
	adminVideo "MediaPortal_2.0/pkg/admin/video"
	"MediaPortal_2.0/pkg/profile"
	appProf "MediaPortal_2.0/pkg/profile/application"
	bookProf "MediaPortal_2.0/pkg/profile/book"
	musicProf "MediaPortal_2.0/pkg/profile/music"
	videoProf "MediaPortal_2.0/pkg/profile/video"
	"MediaPortal_2.0/pkg/user"
	app "MediaPortal_2.0/pkg/user/application"
	"MediaPortal_2.0/pkg/user/book"
	"MediaPortal_2.0/pkg/user/music"
	"MediaPortal_2.0/pkg/user/video"
	"github.com/joho/godotenv"
)

func main() {

	err := godotenv.Load("./../.env")
	if err != nil {
		fmt.Println(err)
		log.Fatal("Error loading .env file")
		return
	}

	dbConn, err := db.Database{}.NewDatabase()
	if err != nil {
		fmt.Println(err)
		return
	}

	repoAdmin := admin.NewRepository(dbConn)
	servAdmin := admin.NewService(repoAdmin)
	handler := admin.NewHandler(servAdmin)

	repoLesson := lesson.NewRepository(dbConn)
	servLesson := lesson.NewService(repoLesson)
	handLesson := lesson.NewHandler(servLesson, servAdmin)

	repoOS := os.NewRepository(dbConn)
	servOS := os.NewService(repoOS)
	handOS := os.NewHandler(servOS, servAdmin)

	adminVideoRepo := adminVideo.NewRepository(dbConn)
	adminVideoServ := adminVideo.NewService(adminVideoRepo)
	adminVideoHand := adminVideo.NewHandler(adminVideoServ, servAdmin)

	adminBookRepo := adminBook.NewRepository(dbConn)
	adminBookServ := adminBook.NewService(adminBookRepo)
	adminBookHand := adminBook.NewHandler(adminBookServ, servAdmin)

	adminAppRepo := adminApp.NewRepository(dbConn)
	adminAppServ := adminApp.NewService(adminAppRepo)
	adminAppHand := adminApp.NewHandler(adminAppServ, servAdmin)

	adminAudioRepo := adminAudio.NewRepository(dbConn)
	adminAudioServ := adminAudio.NewService(adminAudioRepo)
	adminAudioHand := adminAudio.NewHandler(adminAudioServ, servAdmin)

	//site ----------------------------------------------site//
	siteRepo := user.NewRepository(dbConn)
	siteServ := user.NewService(siteRepo)
	siteHandler := user.NewHandler(siteServ)

	repoBook := book.NewRepository(dbConn)
	servBook := book.NewService(repoBook)
	handBook := book.NewHandler(servBook, siteServ)

	repoApp := app.NewRepository(dbConn)
	servApp := app.NewService(repoApp)
	handApp := app.NewHandler(servApp, siteServ)

	repoMusic := music.NewRepository(dbConn)
	servMusic := music.NewService(repoMusic)
	handMusic := music.NewHandler(servMusic, siteServ)

	repoVideo := video.NewRepository(dbConn)
	servVideo := video.NewService(repoVideo)
	handVideo := video.NewHandler(servVideo, siteServ)

	//profile ----------------------------------------profile//
	profRepo := profile.NewRepository(dbConn)
	profServ := profile.NewService(profRepo)
	profHandler := profile.NewHandler(profServ)

	profAppRepo := appProf.NewRepository(dbConn)
	profAppServ := appProf.NewService(profAppRepo)
	profAppHand := appProf.NewHandler(profAppServ, profServ, siteServ)

	profBookRepo := bookProf.NewRepository(dbConn)
	profBookServ := bookProf.NewService(profBookRepo)
	profBookHand := bookProf.NewHandler(profBookServ, profServ, siteServ)

	profMusicRepo := musicProf.NewRepository(dbConn)
	profMusicServ := musicProf.NewService(profMusicRepo)
	profMusicHand := musicProf.NewHandler(profMusicServ, profServ, siteServ)

	profVideoRepo := videoProf.NewRepository(dbConn)
	profVideoServ := videoProf.NewService(profVideoRepo)
	profVideoHand := videoProf.NewHandler(profVideoServ, profServ, siteServ)

	err = http.ListenAndServe("localhost:8080", config.InitRouter(handler, handLesson, handOS, adminVideoHand, adminBookHand, adminAppHand, adminAudioHand, siteHandler, handBook, handApp, handMusic, handVideo, profHandler, profAppHand, profBookHand, profMusicHand, profVideoHand))
	if err != nil {
		return
	}
}
